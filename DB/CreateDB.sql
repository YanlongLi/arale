/*DROP DATABASE arale_db;
CREATE DATABASE arale_db;
USE arale_db;

DROP TABLE tbl_news;
DROP TABLE tbl_user;
*/

DROP TABLE tbl_news;
DROP TABLE tbl_user;
DROP TABLE tbl_schoolmates; 

/*
* 新闻通知表
*/
CREATE TABLE tbl_user
(
	id SERIAL PRIMARY KEY,
	username VARCHAR(64) NOT NULL,
	password VARCHAR(32) NOT NULL
);

CREATE TABLE tbl_news
(
	id SERIAL PRIMARY KEY,
	authorid INT NOT NULL,
	title VARCHAR(128) NOT NULL,
	type INT NOT NULL,
	status INT NOT NULL,
	createtime TIMESTAMP NOT NULL,
	updatetime TIMESTAMP NOT NULL,
	publishtime TIMESTAMP,
	sticktime TIMESTAMP NOT NULL,
	content TEXT NOT NULL,
	FOREIGN KEY(authorid) REFERENCES tbl_user(id) ON DELETE CASCADE
);

CREATE TABLE tbl_schoolmates
(
	id SERIAL PRIMARY KEY,
	graduatetime TIMESTAMP NOT NULL,
	namelist TEXT NOT NULL
);
/*
* Test Data
*/
INSERT INTO  tbl_user
(
	username,
	password
)
VALUES
('admin', 'admin'),
('lili', 'lili'),
('jlh', 'jlh');


INSERT INTO tbl_news
(authorid, title, type, status, createtime, updatetime, publishtime, sticktime, content)
VALUES
/*id=1*/(1,'工程硕士简介', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','工程硕士简介'),
/*id=2*/(1,'招生信息-招生简章', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','2013-10-13 12:12:12', '招生信息-招生简章'),
/*id=3*/(1,'招生信息-报名流程', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','招生信息-报名流程'),
/*id=4*/(1,'招生信息-FAQ', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','招生信息-FAQ'),
/*id=5*/(1,'教学管理-培养方案', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','教学管理-培养方案'),
/*id=6*/(1,'教学管理-课程表', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','教学管理-课程表'),
/*id=7*/(1,'学位论文-论文流程', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','学位论文-论文流程'),
/*id=8*/(1,'院长寄语', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','院长寄语'),
/*id=9*/(1,'blank', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','blank'),
/*id=10*/(1,'blank', 1, 3, '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12', '2013-10-13 12:12:12','blank');
