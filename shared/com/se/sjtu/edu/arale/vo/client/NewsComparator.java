package com.se.sjtu.edu.arale.vo.client;

import java.util.Comparator;

public class NewsComparator implements Comparator<News>
{

	@Override
	public int compare(News o1, News o2)
	{
		boolean bStick1 = o1.isStick();
		boolean bStick2 = o2.isStick();
		if (bStick1 ^ bStick2)
		{// different
			return bStick1 ? -1 : 1;
		}
		return o1.getPublishtime().before(o2.getPublishtime()) ? 1 : -1;

	}

}
