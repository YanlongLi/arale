package com.se.sjtu.edu.arale.vo.client;

import edu.iao.yomi.vo.Column;
import edu.iao.yomi.vo.Table;
import edu.iao.yomi.vo.Vo;

/**
 * 
 * @author lansunlong
 * @since 2013年12月1日
 */
@Table(name="user", code = "tbl_user")
public class User extends Vo
{
	public static final String TABLE = "tbl_user";
	public static final String ID = "id";
	public static final String USER_NAME = "username";
	public static final String PASSWORD = "password";
	
	/*
	 * fields
	 */
	@Column(name = "identifier", code = User.ID, primaryKey = true)
	private Integer id;
	@Column(name = "user name", code = User.USER_NAME)
	private String username;
	@Column(name = "password", code = User.PASSWORD)
	private String password;
	/*
	 * Getters & Setters
	 */
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
		return;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
		return;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
		return;
	}
}
