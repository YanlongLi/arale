package com.se.sjtu.edu.arale.vo.client;

import java.util.Date;

import edu.iao.yomi.vo.Column;
import edu.iao.yomi.vo.Table;
import edu.iao.yomi.vo.Vo;

/**
 * 
 * @author lansunlong
 * @since 2013年12月1日
 */
@Table(name = "news", code = News.TABLE)
public class News extends Vo
{
	
	/*
	 * 
	 */
	public static final String TABLE = "tbl_news";
	public static final String ID = "id";
	public static final String AUTHORID = "authorid";
	public static final String TITLE = "title";
	public static final String TYPE = "type";
	public static final String STATUS = "status";
	public static final String CREATE_TIME = "createtime";
	public static final String UPDATE_TIME = "updatetime";
	public static final String PUBLISH_TIME = "publishtime";
	public static final String STICK_TIME = "sticktime";
	public static final String CONTENT = "content";
	/*
	 * Fields
	 */
	@Column(name = "identifier", code = News.ID, primaryKey = true)
	private Integer id;
	@Column(name = "author id", code = News.AUTHORID)
	private Integer authorid;
	@Column(name = "title", code = News.TITLE)
	private String title;
	@Column(name = "create time", code = News.CREATE_TIME)
	private Date createtime;
	@Column(name = "news type", code = News.TYPE)
	private Integer type;
	@Column(name = "news status", code = News.STATUS)
	private Integer status;
	@Column(name = "update time", code = News.UPDATE_TIME)
	private Date updatetime;
	@Column(name = "publish time", code = News.PUBLISH_TIME)
	private Date publishtime;
	@Column(name = "stick time", code = News.STICK_TIME)
	private Date sticktime;
	@Column(name = "content", code = News.CONTENT)
	private String content;
	//
	private User author;
	/*
	 * News Type
	 */
	public static final int TYPE_DEFAULT = 0x1;
	public static final int TYPE_NEWS = 0x2;
	public static final int TYPE_NOTICE = 0x3;;
	public static final int TYPE_MAJOR= 0x4;
	public static final int TYPE_XNOTICE= 0x5;;
	/*
	 * News Status
	 */
	public static final int STATUS_DEFAULT = 0x1;
	public static final int STATUS_DRAFT = 0x2;
	public static final int STATUS_PUBLISHED = 0x3;
	
	public boolean isStick()
	{
		if (this.sticktime == null || !this.sticktime.after(new Date()))
		{
			return false;
		}
		return true;
	}
	
	/*
	 * Getters & Setters
	 */
	public Integer getId()
	{
		return id;
	}
	
	public void setId(Integer id)
	{
		this.id = id;
		return;
	}
	
	public Integer getAuthorid()
	{
		return authorid;
	}
	
	public void setAuthorid(Integer authorid)
	{
		this.authorid = authorid;
		return;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
		return;
	}
	
	public Date getCreatetime()
	{
		return createtime;
	}
	
	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
		return;
	}
	
	public Date getUpdatetime()
	{
		return updatetime;
	}
	
	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
		return;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public void setContent(String content)
	{
		this.content = content;
		return;
	}
	
	public User getAuthor()
	{
		return author;
	}
	
	public void setAuthor(User author)
	{
		this.author = author;
		return;
	}
	
	public Date getSticktime()
	{
		return sticktime;
	}
	
	public void setSticktime(Date sticktime)
	{
		this.sticktime = sticktime;
		return;
	}
	
	public Integer getType()
	{
		return type;
	}
	
	public void setType(Integer type)
	{
		this.type = type;
		return;
	}
	
	public Integer getStatus()
	{
		return status;
	}
	
	public void setStatus(Integer status)
	{
		this.status = status;
		return;
	}
	
	public Date getPublishtime()
	{
		return publishtime;
	}
	
	public void setPublishtime(Date publishtime)
	{
		this.publishtime = publishtime;
		return;
	}
}
