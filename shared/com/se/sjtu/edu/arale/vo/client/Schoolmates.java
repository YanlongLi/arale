package com.se.sjtu.edu.arale.vo.client;

import java.util.Date;

import edu.iao.yomi.vo.Column;
import edu.iao.yomi.vo.Table;
import edu.iao.yomi.vo.Vo;

@Table(name = "校友目录", code = "tbl_schoolmates")
public class Schoolmates extends Vo
{
	public static String TABLE = "tbl_schoolmates";
	public static String ID = "id";
	public static String GRADUATETIME = "graduatetime";
	public static String NAMELIST = "namelist";

	@Column(name = "ID", code = "id", primaryKey = true)
	private Integer id;
	@Column(name = "毕业时间", code = "graduatetime", allowZero = false)
	private Date graduateTime;
	@Column(name = "名单", code = "namelist", allowZero = false)
	private String namelist;

	/*
	 * Getters & Setters
	 */
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
		return;
	}

	public Date getGraduateTime()
	{
		return graduateTime;
	}

	public void setGraduateTime(Date graduateTime)
	{
		this.graduateTime = graduateTime;
		return;
	}

	public String getNamelist()
	{
		return namelist;
	}

	public void setNamelist(String namelist)
	{
		this.namelist = namelist;
		return;
	}

}
