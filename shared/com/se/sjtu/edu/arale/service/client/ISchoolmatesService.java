package com.se.sjtu.edu.arale.service.client;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.util.AServiceInstance;
import edu.iao.yomi.vo.resp.Response;

public interface ISchoolmatesService extends RemoteService
{
	Response<List<Schoolmates>> getSchoolmatesList();

	Response<Schoolmates> getNameList(Integer yearId);

	Response<String> addSchoolmatesFromExcel(String filePath);
	/*
	 * Instance
	 */
	public static AServiceInstance<ISchoolmatesServiceAsync> INSTANCE = new AServiceInstance<ISchoolmatesServiceAsync>("ISchoolmatesService")
	{
		@Override
		protected ISchoolmatesServiceAsync createInstance()
		{
			return GWT.create(ISchoolmatesService.class);
		}
	};
}
