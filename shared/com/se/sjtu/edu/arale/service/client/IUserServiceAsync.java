package com.se.sjtu.edu.arale.service.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface IUserServiceAsync
{
	void login(String username, String pass, AsyncCallback<Response<String>> callback);
	
	void getCurrentUser(AsyncCallback<Response<User>> callback);
	
	void logout(AsyncCallback<Response<String>> callback);
}
