/**
 * 
 */
package com.se.sjtu.edu.arale.service.client;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.util.AServiceInstance;
import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;
import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface INewsService extends RemoteService
{
	Response<Integer> addNews(News news);
	
	Response<Integer> updateNews(Integer newsId, News news);
	
	Response<String> publishNews(Integer newsId);
	
	Response<News> getNews(Integer newsId);
	
	Response<News> getStaticNews(Integer newsId);
	
	Response<List<News>> getMajorList();
	Response<List<News>> getXNoticeList();
	
	Response<List<News>> getAllNewsByAdmin();
	
	Response<String> removeNews(Integer newsId);
	
	Response<PageList<News>> getNewsList(PageInfo pgInfo);
	
	Response<PageList<News>> getNoticeList(PageInfo pgInfo);
	
	/*
	 * Instance
	 */
	public static AServiceInstance<INewsServiceAsync> INSTANCE = new AServiceInstance<INewsServiceAsync>("INewsService")
	{
		@Override
		protected INewsServiceAsync createInstance()
		{
			return GWT.create(INewsService.class);
		}
	};
}
