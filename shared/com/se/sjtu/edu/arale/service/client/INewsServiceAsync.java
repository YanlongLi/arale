package com.se.sjtu.edu.arale.service.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;
import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface INewsServiceAsync
{
	void getNews(Integer newsId, AsyncCallback<Response<News>> callback);
	
	void getNewsList(PageInfo pgInfo, AsyncCallback<Response<PageList<News>>> callback);
	
	void getNoticeList(PageInfo pgInfo, AsyncCallback<Response<PageList<News>>> callback);
	
	void addNews(News news, AsyncCallback<Response<Integer>> callback);
	
	void updateNews(Integer newsId, News news, AsyncCallback<Response<Integer>> callback);
	
	void removeNews(Integer newsId, AsyncCallback<Response<String>> callback);
	
	void getAllNewsByAdmin(AsyncCallback<Response<List<News>>> callback);

	void publishNews(Integer newsId, AsyncCallback<Response<String>> callback);

	void getStaticNews(Integer newsId, AsyncCallback<Response<News>> callback);

	void getMajorList(AsyncCallback<Response<List<News>>> callback);

	void getXNoticeList(AsyncCallback<Response<List<News>>> callback);
}
