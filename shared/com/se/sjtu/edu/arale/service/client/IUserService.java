/**
 * 
 */
package com.se.sjtu.edu.arale.service.client;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.util.AServiceInstance;
import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface IUserService extends RemoteService
{
	/*
	 * instance
	 */
	public static final AServiceInstance<IUserServiceAsync> INSTANCE = new AServiceInstance<IUserServiceAsync>("IUserService")
	{
		@Override
		protected IUserServiceAsync createInstance()
		{
			return GWT.create(IUserService.class);
		}
	};

	Response<String> login(String username, String pass);

	Response<User> getCurrentUser();
	
	Response<String> logout();
}
