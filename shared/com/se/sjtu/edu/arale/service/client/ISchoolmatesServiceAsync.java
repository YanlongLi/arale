package com.se.sjtu.edu.arale.service.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.vo.resp.Response;

public interface ISchoolmatesServiceAsync
{

	void getSchoolmatesList(AsyncCallback<Response<List<Schoolmates>>> callback);

	void getNameList(Integer yearId, AsyncCallback<Response<Schoolmates>> callback);

	void addSchoolmatesFromExcel(String filePath, AsyncCallback<Response<String>> callback);

}
