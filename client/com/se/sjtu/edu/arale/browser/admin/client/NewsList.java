package com.se.sjtu.edu.arale.browser.admin.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.service.client.ISchoolmatesService;
import com.se.sjtu.edu.arale.service.client.IUserService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.event.CompleteEvent;
import edu.iao.yomi.browser.util.client.form.FileUpload;
import edu.iao.yomi.browser.util.client.form.button.Button;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;
import edu.iao.yomi.vo.resp.ResponseAdapter;

/**
 * 
 * @author lansunlong
 * @since 2013年12月4日
 */
public class NewsList extends Composite
{
	interface NewsListUiBinder extends UiBinder<Widget, NewsList>
	{
	}

	private static NewsListUiBinder uiBinder = GWT.create(NewsListUiBinder.class);
	@UiField
	Button btnNew;
	@UiField
	VerticalPanel vpContent;
	@UiField
	HyperLink btnLogout;
	@UiField
	HyperLink btnIndexPage;
	@UiField
	DeckPanel deckPanel;
	@UiField
	FileUpload btnUpload;
	//
	private static NewsListItemHead itemHead = new NewsListItemHead();

	public NewsList()
	{
		this.initWidget(NewsList.uiBinder.createAndBindUi(this));
		this.btnUpload.setAction(GWT.getHostPageBaseURL() + "ISchoolmatesService?action=uploadExcel&fileType=xls");
		return;
	}

	@Override
	public void onLoad()
	{
		this.deckPanel.setAnimationEnabled(true);
		this.helpRefresh();
		// Window.alert(this.deckPanel.getWidgetCount() + "");
		this.deckPanel.showWidget(0);
		return;
	}

	/*
	 * help method
	 */
	private void helpSetContent(List<News> lst)
	{
		this.vpContent.clear();
		this.vpContent.add(NewsList.itemHead);
		// Window.alert(lst.size() + "");
		//
		for (News news : lst)
		{
			NewsListItem item = new NewsListItem(news);
			item.addActionHandler(new ActionHandler()
			{
				@Override
				public void onAction(ActionEvent event)
				{
					Integer newsId = ((NewsListItem) event.getSource()).getId();
					NewsList.this.helpAddEditor(newsId);
					return;
				}
			});
			this.vpContent.add(item);
		}
		return;
	}

	/*
	 * help method
	 */
	private void helpRefresh()
	{
		this.vpContent.clear();
		INewsService.INSTANCE.get().getAllNewsByAdmin(new ResponseAdapter<List<News>>()
		{
			@Override
			public void onClear(List<News> lst)
			{
				NewsList.this.helpSetContent(lst);
			}

			@Override
			public void onError(String info)
			{
				GWT.log("error" + info);
				super.onError(info);
			}
		});
		return;
	}

	private void helpAddEditor(Integer newsId)
	{
		PaneEditNews editor = null;
		if (newsId == null)
		{
			editor = new PaneEditNews();
		}
		else
		{
			editor = new PaneEditNews(newsId);
		}
		editor.addActionHandler(new ActionHandler()
		{
			@Override
			public void onAction(ActionEvent event)
			{
				NewsList.this.helpRefresh();
				NewsList.this.deckPanel.showWidget(0);
				return;
			}
		});
		this.deckPanel.add(editor);
		this.deckPanel.showWidget(1);
		return;
	}

	/*
	 * event handler
	 */
	@UiHandler("btnNew")
	void onBtnNewClick(ClickEvent event)
	{
		this.helpAddEditor(null);
		return;
	}

	@UiHandler("btnLogout")
	void onBtnLogoutClick(ClickEvent event)
	{
		IUserService.INSTANCE.get().logout(new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String content)
			{
				Window.alert("退出成功");
				Window.Location.reload();
				return;
			}

			@Override
			public void onError(String info)
			{
				super.onError(info);
			}
		});
		return;
	}

	@UiHandler("btnIndexPage")
	void onBtnIndexPageClick(ClickEvent event)
	{
		Window.open(GWT.getHostPageBaseURL() + "Index.html", "_blank", "");
		return;
	}

	@UiHandler("btnUpload")
	void onBtnUploadChange(ChangeEvent event)
	{
		String fileName = ((FileUpload) event.getSource()).getFilename();
		if (TextUtil.isNullOrZero(fileName))
		{
			return;
		}
		this.btnUpload.doUpload();
		return;
	}

	@UiHandler("btnUpload")
	void onBtnUploadComplete(CompleteEvent event)
	{
		String result = ((FileUpload) event.getSource()).getUploadResult();
		if (!result.startsWith("success"))
		{
			Window.alert("上传失败\n" + result.substring("error : ".length()));
			return;
		}
		if (!Window.confirm("上传成功，是否进行导入？"))
		{
			return;
		}
		String filePath = result.substring("success : ".length());
		ISchoolmatesService.INSTANCE.get().addSchoolmatesFromExcel(filePath, new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String res)
			{
				Window.alert("导入成功");
				return;
			}

			@Override
			public void onError(String err)
			{
				Window.alert("导入失败" + err);
				return;
			}
		});
		return;
	}
}
