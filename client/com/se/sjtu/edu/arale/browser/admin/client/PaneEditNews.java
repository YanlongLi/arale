package com.se.sjtu.edu.arale.browser.admin.client;

import java.util.Date;

import scu.edu.plugin.ke.client.KindEditorWidget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.Format;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.se.sjtu.edu.arale.browser.global.client.GlobalUtil;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.event.intr.IHasActionHandlers;
import edu.iao.yomi.browser.util.client.form.button.Button;
import edu.iao.yomi.browser.util.client.form.input.TextInput;
import edu.iao.yomi.browser.util.client.form.select.ComboSelect;
import edu.iao.yomi.vo.resp.ResponseAdapter;

/**
 * 
 * @author lansunlong
 * @since 2013年12月5日
 */
public class PaneEditNews extends Composite implements IHasActionHandlers
{
	interface PaneEditNewsUiBinder extends UiBinder<Widget, PaneEditNews>
	{
	}

	private static PaneEditNewsUiBinder uiBinder = GWT.create(PaneEditNewsUiBinder.class);
	@UiField
	TextInput txtTitle;
	@UiField
	ComboSelect<Integer> cmbType;

	@UiField(provided = true)
	DateBox timeStick;
	@UiField
	KindEditorWidget ke;
	@UiField
	Button btnPublish;
	@UiField
	Button btnSave;
	@UiField
	Button btnBack;
	//
	private Integer newsId = null;

	public PaneEditNews()
	{
		this.timeStick = new DateBox(new DatePicker(), null, fm);
		this.initWidget(PaneEditNews.uiBinder.createAndBindUi(this));
		this.cmbType.clear();
		this.cmbType.addItem("新闻", News.TYPE_NEWS);
		this.cmbType.addItem("通知公告", News.TYPE_NOTICE);
		this.cmbType.addItem("专业方向", News.TYPE_MAJOR);
		this.cmbType.addItem("招生通知", News.TYPE_XNOTICE);
		this.cmbType.setItemSelected(1, true);
		return;
	}

	public PaneEditNews(Integer newsId)
	{
		this.timeStick = new DateBox(new DatePicker(), null, fm);
		this.initWidget(PaneEditNews.uiBinder.createAndBindUi(this));
		this.cmbType.clear();
		this.cmbType.addItem("新闻", News.TYPE_NEWS);
		this.cmbType.addItem("通知公告", News.TYPE_NOTICE);
		this.cmbType.addItem("专业方向", News.TYPE_MAJOR);
		this.cmbType.addItem("招生通知", News.TYPE_XNOTICE);
		this.cmbType.setItemSelected(0, true);
		this.newsId = newsId;
		return;
	}

	private final Format fm = new Format()
	{
		private final DateTimeFormat dateTimeFormate = DateTimeFormat.getFormat("yyyy年MM月dd日");

		@Override
		public void reset(DateBox dateBox, boolean abandon)
		{
			return;
		}

		@Override
		public Date parse(DateBox dateBox, String text, boolean reportError)
		{
			Date date = null;
			try
			{
				if (text.length() > 0)
				{
					date = this.dateTimeFormate.parse(text);
				}
			}
			catch (IllegalArgumentException e)
			{
				return null;
			}
			return date;
		}

		@Override
		public String format(DateBox dateBox, Date date)
		{
			if (date == null)
			{
				return "";
			}
			return this.dateTimeFormate.format(date);
		}
	};

	@Override
	public void onLoad()
	{

		if (this.newsId == null)
		{
			return;
		}
		INewsService.INSTANCE.get().getNews(this.newsId, new ResponseAdapter<News>()
		{
			@Override
			public void onClear(News content)
			{

				PaneEditNews.this.helpInitData(content);
				return;
			}

			@Override
			public void onError(String info)
			{
				GWT.log(info);
				Window.alert(info);
				return;
			}
		});
		return;
	}

	private Integer status = News.STATUS_DRAFT;

	/*
	 * help method
	 */
	private void helpInitData(News news)
	{

		if (news.getType() == News.TYPE_DEFAULT)
		{
			this.cmbType.setSelectedItem(-1);
		}
		else
		{
			int index = 0;
			for (index = 0; index < this.cmbType.getItemCount(); index++)
			{
				Integer value = this.cmbType.getValue(index);
				if (value.equals(news.getType()))
				{
					break;
				}
			}
			this.cmbType.setSelectedItem(index);
		}
		this.txtTitle.setText(news.getTitle());
		this.timeStick.getDatePicker().setValue(news.getSticktime(), true);
		this.ke.html(GlobalUtil.htmlSpecialCharsReverse(news.getContent()));
		if (news.getStatus() == News.STATUS_PUBLISHED)
		{
			this.status = News.STATUS_PUBLISHED;
			this.btnPublish.setEnabled(false);
		}
		return;
	}

	private News helpFetchData()
	{

		String title = this.txtTitle.getText().trim();
		if (TextUtil.isNullOrZero(title))
		{
			Window.alert("怎么能没有标题呢~~~");
			return null;
		}
		Integer type = News.TYPE_DEFAULT;
		int index = this.cmbType.getSelectedIndex();
		if (index != -1)
		{
			type = this.cmbType.getValue(index);
		}
		Date stickDate = this.timeStick.getValue();
		if (stickDate == null)
		{
			GWT.log("StickTime == null");
			stickDate = new Date();
		}
		GWT.log(DateTimeFormat.getFormat("yyyy-MM-dd").format(stickDate));
		//
		this.ke.sync();
		String content = this.ke.getText();
		if (TextUtil.isNullOrZero(content))
		{
			Window.alert("您没有输入任何内容哦-_-!");
			return null;
		}
		//
		//
		News news = new News();
		news.setTitle(title);
		content = KindEditorWidget.htmlSpecialChars(content);
		// content = content.replace("\n", " ");
		// content = content.replace("\\s+", " ");
		news.setContent(content);
		news.setType(type);
		news.setStatus(this.status);
		news.setSticktime(stickDate);
		return news;
	}

	/*
	 * event handler
	 */
	@UiHandler("btnBack")
	void onBtnBack(ClickEvent event)
	{

		if (!Window.confirm("返回前请手动保存数据，确定返回？"))
		{
			return;
		}
		this.removeFromParent();
		this.fireEvent(new ActionEvent());
		return;
	}

	@UiHandler("btnSave")
	void onBtnSaveClick(ClickEvent event)
	{

		News news = this.helpFetchData();
		if (news == null)
		{
			return;
		}
		news.setId(null);
		ResponseAdapter<Integer> ra = new ResponseAdapter<Integer>()
		{
			@Override
			public void onClear(Integer content)
			{

				// Window.alert(content);
				PaneEditNews.this.newsId = content;
				Window.alert("保存成功");
				return;
			}

			@Override
			public void onError(String info)
			{

				GWT.log(info);
				super.onError(info);
			}
		};
		if (this.newsId == null)
		{
			INewsService.INSTANCE.get().addNews(news, ra);
		}
		else
		{
			INewsService.INSTANCE.get().updateNews(this.newsId, news, ra);
		}
		return;
	}

	@UiHandler("btnPublish")
	void onBtnPublishClick(ClickEvent event)
	{

		if (this.newsId == null)
		{
			Window.alert("请先保存再发布");
			return;
		}
		INewsService.INSTANCE.get().publishNews(this.newsId, new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String content)
			{

				Window.alert("发布成功");
				PaneEditNews.this.btnPublish.setEnabled(false);
			}

			@Override
			public void onError(String info)
			{

				GWT.log(info);
				Window.alert(info);
			}
		});
		return;
	}

	@Override
	public HandlerRegistration addActionHandler(ActionHandler handler)
	{

		return this.addHandler(handler, ActionEvent.getType());
	}
}
