package com.se.sjtu.edu.arale.browser.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.event.intr.IHasActionHandlers;
import edu.iao.yomi.browser.util.client.form.button.Button;
import edu.iao.yomi.browser.util.client.form.label.Label;
import edu.iao.yomi.vo.resp.ResponseAdapter;

/**
 * 
 * @author lansunlong
 * @since 2013年12月4日
 */
public class NewsListItem extends Composite implements IHasActionHandlers
{
	interface NewsListItemUiBinder extends UiBinder<Widget, NewsListItem>
	{
	}
	
	public static final DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");
	private static NewsListItemUiBinder uiBinder = GWT.create(NewsListItemUiBinder.class);
	@UiField
	Label lblTitle;
	@UiField
	Label lblAuthor;
	@UiField
	Label lblPublishTime;
	@UiField
	Label lblCreateTime;
	@UiField
	Button btnModify;
	@UiField
	Button btnDelete;
	@UiField
	Label lblType;
	@UiField
	Label lblStatus;
	//
	News news;
	
	public NewsListItem()
	{
		this.initWidget(NewsListItem.uiBinder.createAndBindUi(this));
	}
	
	public NewsListItem(News news)
	{
		this.initWidget(NewsListItem.uiBinder.createAndBindUi(this));
		this.news = news;
		return;
	}
	
	@Override
	public void onLoad()
	{
		this.helpInit();
		return;
	}
	
	public Integer getId()
	{
		return this.news.getId();
	}
	
	private void helpInit()
	{
		this.lblTitle.setText(this.news.getTitle());
		this.lblTitle.setTitle(this.news.getTitle());
		String type = "";
		switch(this.news.getType())
		{
		case News.TYPE_NEWS:
			type = "新闻";
			break;
		case News.TYPE_NOTICE:
			type = "通知公告";
			break;
		case News.TYPE_MAJOR:
			type = "专业方向";
			break;
		case News.TYPE_XNOTICE:
			type = "招生通知";
			break;
		}
		this.lblType.setText(type);
		this.lblAuthor.setText(this.news.getAuthor().getUsername());
		this.lblCreateTime.setText(NewsListItem.dtf.format(this.news.getCreatetime()));
		String pubTime = (this.news.getStatus() == News.STATUS_DRAFT) ? "-" : NewsListItem.dtf.format(this.news.getPublishtime());
		this.lblPublishTime.setText(pubTime);
		String status = (this.news.getStatus() == News.STATUS_DRAFT) ? "未发布" : "已发布";
		this.lblStatus.setText(status);
		this.lblStatus.setTitle(status);
		//
		if(this.news.getId() < 11)
		{
			this.btnDelete.setEnabled(false);
		}
		return;
	}
	
	@UiHandler("btnModify")
	void onBtnModifyClick(ClickEvent event)
	{
		NewsListItem.this.fireEvent(new ActionEvent());//
		return;
	}
	
	@UiHandler("btnDelete")
	void onBtnDeleteClick(ClickEvent event)
	{
		INewsService.INSTANCE.get().removeNews(this.news.getId(), new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String content)
			{
				NewsListItem.this.removeFromParent();
				return;
			}
			
			@Override
			public void onError(String info)
			{
				GWT.log(info);
				super.onError(info);
			}
		});
		return;
	}
	
	/**
	 * <pre>
	 * fire when modify
	 * </pre>
	 */
	@Override
	public HandlerRegistration addActionHandler(ActionHandler handler)
	{
		return this.addHandler(handler, ActionEvent.getType());
	}
}
