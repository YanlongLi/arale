package com.se.sjtu.edu.arale.browser.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.IUserService;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.browser.util.client.form.button.Button;
import edu.iao.yomi.browser.util.client.form.input.PasswordInput;
import edu.iao.yomi.browser.util.client.form.input.TextInput;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class PaneLogin extends Composite
{
	private static PaneLoginUiBinder uiBinder = GWT.create(PaneLoginUiBinder.class);
	@UiField
	Button btnLogin;
	@UiField
	TextInput txtUsername;
	@UiField
	PasswordInput txtPass;
	@UiField
	Button btnClear;
	
	interface PaneLoginUiBinder extends UiBinder<Widget, PaneLogin>
	{
	}
	
	public PaneLogin()
	{

		this.initWidget(PaneLogin.uiBinder.createAndBindUi(this));
	}
	
	/*
	 * event handler
	 */
	@UiHandler("btnLogin")
	void onBtnLoginClick(ClickEvent event)
	{

		String username = this.txtUsername.getText();
		if(TextUtil.isNullOrZero(username))
		{
			Window.alert("用户名不能为空");
			return;
		}
		String pass = this.txtPass.getText();
		if(TextUtil.isNullOrZero(pass))
		{
			Window.alert("密码不能为空");
			return;
		}
		IUserService.INSTANCE.get().login(username, pass, new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String content)
			{
				Window.Location.reload();
			}
		});
		return;
	}

	@UiHandler("btnClear")
	void onBtnClearClick(ClickEvent event)
	{
	
		this.txtPass.setText("");
		this.txtUsername.setText("");
		return;
	}
}
