package com.se.sjtu.edu.arale.browser.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class NewsListItemHead extends Composite
{
	private static NewsListItemHeadUiBinder uiBinder = GWT.create(NewsListItemHeadUiBinder.class);
	
	interface NewsListItemHeadUiBinder extends UiBinder<Widget, NewsListItemHead>
	{
	}
	
	public NewsListItemHead()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
}
