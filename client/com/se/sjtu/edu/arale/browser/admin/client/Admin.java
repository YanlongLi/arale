/**
 * 
 */
package com.se.sjtu.edu.arale.browser.admin.client;

import com.google.gwt.core.client.EntryPoint;
import com.se.sjtu.edu.arale.service.client.IUserService;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.browser.layout.client.FullWindowPanel;
import edu.iao.yomi.vo.resp.ResponseAdapter;

//import edu.iao.yomi.browser.util.client.form.button.Button;
/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public class Admin implements EntryPoint
{
	private final FullWindowPanel root = new FullWindowPanel();

	@Override
	public void onModuleLoad()
	{
		// Admin.this.root.add(new PaneTest());
		IUserService.INSTANCE.get().getCurrentUser(new ResponseAdapter<User>()
		{
			@Override
			public void onClear(User content)
			{
				Admin.this.root.clear();
				Admin.this.root.add(new NewsList());
				return;
			}

			@Override
			public void onError(String info)
			{
				Admin.this.root.clear();
				Admin.this.root.add(new PaneLogin());
				return;
			}
		});
	}
}
