package com.se.sjtu.edu.arale.browser.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.ISchoolmatesService;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.browser.util.client.event.CompleteEvent;
import edu.iao.yomi.browser.util.client.form.FileUpload;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class PaneUploadSmList extends Composite
{

	private static PaneUploadSmListUiBinder uiBinder = GWT.create(PaneUploadSmListUiBinder.class);
	@UiField
	FileUpload btnUpload;

	interface PaneUploadSmListUiBinder extends UiBinder<Widget, PaneUploadSmList>
	{
	}

	public PaneUploadSmList()
	{
		initWidget(uiBinder.createAndBindUi(this));
		this.btnUpload.setAction(GWT.getHostPageBaseURL() + "ISchoolmatesService?action=uploadExcel&fileType=xls");
		return;
	}

	@UiHandler("btnUpload")
	void onBtnUploadChange(ChangeEvent event)
	{
		String fileName = ((FileUpload) event.getSource()).getFilename();
		if (TextUtil.isNullOrZero(fileName))
		{
			return;
		}
		this.btnUpload.doUpload();
		return;
	}

	@UiHandler("btnUpload")
	void onBtnUploadComplete(CompleteEvent event)
	{
		String result = ((FileUpload) event.getSource()).getUploadResult();
		if (!result.startsWith("success"))
		{
			Window.alert("上传失败\n" + result.substring("error : ".length()));
			return;
		}
		if (!Window.confirm("上传成功，是否进行导入？"))
		{
			return;
		}
		String filePath = result.substring("success : ".length());
		ISchoolmatesService.INSTANCE.get().addSchoolmatesFromExcel(filePath, new ResponseAdapter<String>()
		{
			@Override
			public void onClear(String res)
			{
				Window.alert("导入成功");
				return;
			}

			@Override
			public void onError(String err)
			{
				Window.alert("导入失败" + err);
				return;
			}
		});
		return;
	}
}
