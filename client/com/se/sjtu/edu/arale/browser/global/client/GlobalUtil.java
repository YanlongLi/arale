package com.se.sjtu.edu.arale.browser.global.client;

public class GlobalUtil
{
	public static String htmlSpecialChars(String str)
	{
		str = str.replaceAll("&", "&amp;");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		str = str.replaceAll("\"", "&quot;");
		return str;
	}
	
	public static String htmlSpecialCharsReverse(String str)
	{
		str = str.replaceAll("&amp;", "&");
		str = str.replaceAll("&lt;", "<");
		str = str.replaceAll("&gt;", ">");
		str = str.replaceAll("&quot;", "\"");
		// str = str.replaceAll("&nbsp;\\s&nbsp;", "　");
		return str;
	}
}
