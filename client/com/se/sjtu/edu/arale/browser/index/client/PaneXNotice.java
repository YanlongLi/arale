package com.se.sjtu.edu.arale.browser.index.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.browser.index.client.news.NewsItem;
import com.se.sjtu.edu.arale.browser.index.client.news.PaneNewsDetail;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.form.label.Label;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class PaneXNotice extends Composite
{
	interface PaneXNoticeUiBinder extends UiBinder<Widget, PaneXNotice>
	{
	}

	private static PaneXNoticeUiBinder uiBinder = GWT.create(PaneXNoticeUiBinder.class);
	@UiField
	VerticalPanel content;
	@UiField
	Label btnBack;
	Widget leftPanel;
	Widget w;

	public PaneXNotice()
	{
		this.initWidget(PaneXNotice.uiBinder.createAndBindUi(this));
	}

	public PaneXNotice(Widget w)
	{
		this.initWidget(PaneXNotice.uiBinder.createAndBindUi(this));
		this.leftPanel = w;
		this.w = w;
		return;
	}

	@Override
	public void onLoad()
	{
		INewsService.INSTANCE.get().getXNoticeList(new ResponseAdapter<List<News>>()
		{
			@Override
			public void onClear(List<News> content)
			{
				PaneXNotice.this.helpShowNews(content);
				return;
			}

			@Override
			public void onError(String info)
			{
				return;
			}
		});
		return;
	}

	private void helpShowNews(List<News> lst)
	{
		this.content.clear();//
		for (News n : lst)
		{
			NewsItem link = new NewsItem(n);
			link.addActionHandler(new ActionHandler()
			{
				@Override
				public void onAction(ActionEvent event)
				{
					News ne = ((NewsItem) event.getSource()).getNews();
					((HasWidgets) PaneXNotice.this.leftPanel).clear();
					((HasWidgets) PaneXNotice.this.leftPanel).add(new PaneNewsDetail(ne, "招生信息->通知公告", GWT.getHostPageBaseURL() + "Index.html?pid=12"));
					return;
				}
			});
			this.content.add(link);
		}
		return;
	}

}
