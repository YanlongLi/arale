package com.se.sjtu.edu.arale.browser.index.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.browser.index.client.news.NewsItem;
import com.se.sjtu.edu.arale.browser.index.client.news.PaneNewsDetail;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.form.label.Label;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class PaneMajor extends Composite
{
	private static PaneMajorUiBinder uiBinder = GWT.create(PaneMajorUiBinder.class);
	@UiField
	VerticalPanel content;
	@UiField Label btnBack;
	
	interface PaneMajorUiBinder extends UiBinder<Widget, PaneMajor>
	{
	}
	
	Widget leftPanel;
	
	public PaneMajor()
	{
		this.initWidget(PaneMajor.uiBinder.createAndBindUi(this));
	}
	
	public PaneMajor(Widget w)
	{
		this.initWidget(PaneMajor.uiBinder.createAndBindUi(this));
		this.leftPanel = w;
	}
	
	@Override
	public void onLoad()
	{
		INewsService.INSTANCE.get().getMajorList(new ResponseAdapter<List<News>>()
		{
			@Override
			public void onClear(List<News> content)
			{
				PaneMajor.this.helpShowNews(content);
				return;
			}
			
			@Override
			public void onError(String info)
			{
				return;
			}
		});
		return;
	}
	
	private void helpShowNews(List<News> lst)
	{
		this.content.clear();//
		for(News n : lst)
		{
			NewsItem link = new NewsItem(n);
			link.addActionHandler(new ActionHandler()
			{
				@Override
				public void onAction(ActionEvent event)
				{
					News ne = ((NewsItem)event.getSource()).getNews();
					((HasWidgets)PaneMajor.this.leftPanel).clear();
					((HasWidgets) PaneMajor.this.leftPanel).add(new PaneNewsDetail(ne, "招生信息->专业方向", GWT.getHostPageBaseURL() + "Index.html?pid=11"));
					return;
				}
			});
			this.content.add(link);
		}
		return;
	}
}
