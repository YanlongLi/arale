package com.se.sjtu.edu.arale.browser.index.client;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.browser.index.client.news.PaneNews;
import com.se.sjtu.edu.arale.browser.index.client.schoolmate.PaneSchoolmateCate;

import edu.iao.yomi.browser.layout.client.RightFloatLayout;
import edu.iao.yomi.browser.layout.client.StaticLayout;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;
import edu.iao.yomi.browser.util.client.form.label.Label;

/**
 * 
 * @author lansunlong
 * @since 2013年11月30日
 */
public class BasePanel extends Composite
{
	interface BasePanelUiBinder extends UiBinder<Widget, BasePanel>
	{
	}

	/*
	 * Cookie
	 */
	public static final int COOKIE_EXPIRE_HOURS = 1;
	// public static final long MILLISECS_PER_HOUR = 1000L * 60L * 60L;
	public static final long MILLISECS_PER_HOUR = 1000L * 60L;
	public static final String domainName = "219.228.111.138";
	public static final String cookieName = "view_count";
	/*
	 * 
	 */
	private static BasePanelUiBinder uiBinder = GWT.create(BasePanelUiBinder.class);
	@UiField
	RightFloatLayout contentPane;
	@UiField
	MenuBar menu;
	//
	@UiField
	MenuItem indexMenuItem;
	//
	@UiField
	MenuItem gctMemuItem;
	//
	@UiField
	MenuBar enrollInfoMenu;
	@UiField
	MenuItem enrollInfoBriefItem;
	@UiField
	MenuItem enrollInfoApplyItem;
	@UiField
	MenuItem enrollInfoMajorItem;
	@UiField
	MenuItem enrollInfoNoticeItem;
	@UiField
	MenuItem enrollInfoFaqItem;
	//
	@UiField
	MenuBar teachItem;
	@UiField
	MenuItem teachDocumentItem;
	@UiField
	MenuItem teachProgramItem;
	@UiField
	MenuItem teachCourseItem;
	//
	@UiField
	MenuBar paperMenu;
	@UiField
	MenuItem paperProcessItem;
	@UiField
	MenuItem paperBeginItem;
	@UiField
	MenuItem paperMedCheckItem;
	@UiField
	MenuItem paperTemplateItem;
	@UiField
	MenuItem paperPptItem;
	//
	@UiField
	MenuItem elearingItem;
	//
	//
	@UiField
	MenuBar schoolmateMenu;
	@UiField
	MenuItem schoolmateCateItem;
	@UiField
	MenuItem schoolmateDeanMessageItem;
	@UiField
	StaticLayout downloadBlock;
	@UiField
	Label lblViewCount;
	@UiField HyperLink sjtuLogo;

	//

	private final Map<Integer, Widget> paneMap = new HashMap<Integer, Widget>();
	Integer firtId = 10;

	public BasePanel(Integer pid)
	{
		this.initWidget(BasePanel.uiBinder.createAndBindUi(this));
		this.firtId = pid;
		return;
	}

	@Override
	public void onLoad()
	{
		this.menu.setAutoOpen(true);
		this.menu.setAnimationEnabled(true);
		this.sjtuLogo.setUrl(GWT.getHostPageBaseURL() + "Index.html");
		//
		this.helpInitIndexMenu();
		this.helpInitGctItem();
		this.helpInitEnrollMenu();
		this.helpInitTeachMenu();
		this.helpInitPaperMenu();
		this.helpInitElearningMenu();
		this.helpInitSchoolmateMenu();
		//
		// this.indexMenuItem.getScheduledCommand().execute();
		if (!this.paneMap.containsKey(this.firtId))
		{
			this.firtId = 10;
		}
		Widget w = this.paneMap.get(this.firtId);
		this.contentPane.add(w);
		this.helpModifyDownload();
		//
		this.helpSetViewCount();
		//
		return;
	}

	private void helpSetViewCount()
	{
		final String strCount = Cookies.getCookie(BasePanel.cookieName);
		if (strCount == null || "".equals(strCount) || "undefined".equals(strCount))
		{
			final Date expires = new Date();
			expires.setTime(expires.getTime() + BasePanel.MILLISECS_PER_HOUR * BasePanel.COOKIE_EXPIRE_HOURS);
			String servletPath = GWT.getHostPageBaseURL() + "ViewCountServlet";
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, servletPath);
			rb.setCallback(new RequestCallback()
			{
				@Override
				public void onResponseReceived(Request request, Response response)
				{
					if (response.getStatusCode() != HttpStatus.SC_OK)
					{
						BasePanel.this.lblViewCount.setText("status wrong" + response.getStatusText());
						return;
					}
					String res = response.getText();
					BasePanel.this.lblViewCount.setText("访问人数：" + res);
				}

				@Override
				public void onError(Request request, Throwable exception)
				{
					Window.alert("get view count error");
				}
			});
			try
			{
				rb.send();
			}
			catch (RequestException e)
			{
				Window.alert("request exception");
			}
		}
		else
		{
			BasePanel.this.lblViewCount.setText("访问人数：" + strCount);
		}
		return;
	}

	private void helpModifyDownload()
	{
		// int count = this.downloadBlock.getWidgetCount();
		// for(int i = 0; i < count; i ++)
		// {
		// HyperLink link = (HyperLink)this.downloadBlock.getWidget(i);
		// String url = link.getUrl();
		// if(url.startsWith("./Download/"))
		// {
		// link.setUrl(GWT.getModuleBaseURL() + "FileDownload?filename=" + url);
		// }
		// }
	}

	//
	// init menu
	//
	private void helpInitIndexMenu()
	{
		BasePanel.this.paneMap.put(10, new PaneNews(BasePanel.this.contentPane));
		this.indexMenuItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(10))
				{
					BasePanel.this.paneMap.put(10, new PaneNews(BasePanel.this.contentPane));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(10));
				return;
			}
		});
		return;
	}

	private void helpInitGctItem()
	{
		BasePanel.this.paneMap.put(1, new StaticHtmlBase(1, "首页->工程硕士简介"));
		this.gctMemuItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(1))
				{
					BasePanel.this.paneMap.put(1, new StaticHtmlBase(1, "首页->工程硕士简介"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(1));
				//
				return;
			}
		});
		return;
	}

	private void helpInitEnrollMenu()
	{
		this.enrollInfoMenu.setAutoOpen(true);
		this.enrollInfoMenu.setAnimationEnabled(true);
		//
		BasePanel.this.paneMap.put(2, new StaticHtmlBase(2, "招生信息->招生简章"));
		this.enrollInfoBriefItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(2))
				{
					BasePanel.this.paneMap.put(2, new StaticHtmlBase(2, "招生信息->招生简章"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(2));
				//
				return;
			}
		});

		BasePanel.this.paneMap.put(3, new StaticHtmlBase(3, "招生信息->报名流程"));
		this.enrollInfoApplyItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(3))
				{
					BasePanel.this.paneMap.put(3, new StaticHtmlBase(3, "招生信息->报名流程"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(3));
				//
				return;
			}
		});
		BasePanel.this.paneMap.put(11, new PaneMajor(BasePanel.this.contentPane));
		this.enrollInfoMajorItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(11))
				{
					BasePanel.this.paneMap.put(11, new PaneMajor(BasePanel.this.contentPane));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(11));
				return;
			}
		});
		BasePanel.this.paneMap.put(12, new PaneXNotice(BasePanel.this.contentPane));
		this.enrollInfoNoticeItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(12))
				{
					BasePanel.this.paneMap.put(12, new PaneXNotice(BasePanel.this.contentPane));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(12));
				return;
			}
		});
		BasePanel.this.paneMap.put(4, new StaticHtmlBase(4, "招生信息->FAQ"));
		this.enrollInfoFaqItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(4))
				{
					BasePanel.this.paneMap.put(4, new StaticHtmlBase(4, "招生信息->FAQ"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(4));
				//
				return;
			}
		});;
		return;
	}

	private void helpInitTeachMenu()
	{
		this.teachItem.setAutoOpen(true);
		this.teachItem.setAnimationEnabled(true);
		//
		this.teachDocumentItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open("http://www.gs.sjtu.edu.cn/publicAffair/paShow.ahtml?id=24", "_blank", "");
				return;
			}
		});
		BasePanel.this.paneMap.put(5, new StaticHtmlBase(5, "教学管理->培养方案"));
		this.teachProgramItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(5))
				{
					BasePanel.this.paneMap.put(5, new StaticHtmlBase(5, "教学管理->培养方案"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(5));
				//
				return;
			}
		});
		BasePanel.this.paneMap.put(6, new StaticHtmlBase(6, "教学管理->课程表"));
		this.teachCourseItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(6))
				{
					BasePanel.this.paneMap.put(6, new StaticHtmlBase(6, "教学管理->课程表"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(6));
				return;
			}
		});
		return;
	}

	private void helpInitPaperMenu()
	{
		this.paperMenu.setAutoOpen(true);
		this.paperMenu.setAnimationEnabled(true);
		//
		BasePanel.this.paneMap.put(7, new StaticHtmlBase(7, "学位论文->论文流程"));
		this.paperProcessItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(7))
				{
					BasePanel.this.paneMap.put(7, new StaticHtmlBase(7, "学位论文->论文流程"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(7));
				return;
			}
		});
		this.paperBeginItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open(GWT.getHostPageBaseURL() + "Download/2.doc", "_blank", "");
				return;
			}
		});
		this.paperMedCheckItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open(GWT.getHostPageBaseURL() + "Download/7.doc", "_blank", "");
				return;
			}
		});
		this.paperTemplateItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open(GWT.getHostPageBaseURL() + "Download/4.doc", "_blank", "");
				return;
			}
		});
		this.paperPptItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open(GWT.getHostPageBaseURL() + "Download/5.ppt", "_blank", "");
				return;
			}
		});
		return;
	}

	private void helpInitElearningMenu()
	{
		this.elearingItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				Window.open("http://backup.se.sjtu.edu.cn/elearning/login.asp", "_blank", "");
				return;
			}
		});
		return;
	}

	private void helpInitSchoolmateMenu()
	{
		this.schoolmateMenu.setAutoOpen(true);
		this.schoolmateMenu.setAnimationEnabled(true);
		//
		BasePanel.this.paneMap.put(13, new PaneSchoolmateCate());
		this.schoolmateCateItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(13))
				{
					BasePanel.this.paneMap.put(13, new PaneSchoolmateCate());
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(13));
				return;
			}
		});
		BasePanel.this.paneMap.put(8, new StaticHtmlBase(8, "首页->校长寄语"));
		this.schoolmateDeanMessageItem.setScheduledCommand(new ScheduledCommand()
		{
			@Override
			public void execute()
			{
				BasePanel.this.contentPane.clear();
				if (!BasePanel.this.paneMap.containsKey(8))
				{
					BasePanel.this.paneMap.put(8, new StaticHtmlBase(8, "首页->校长寄语"));
				}
				BasePanel.this.contentPane.add(BasePanel.this.paneMap.get(8));
				return;
			}
		});
		return;
	}
}
