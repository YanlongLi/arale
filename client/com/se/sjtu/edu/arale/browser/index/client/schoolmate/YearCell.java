package com.se.sjtu.edu.arale.browser.index.client.schoolmate;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;

import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.event.intr.IHasActionHandlers;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;

public class YearCell extends HyperLink implements IHasActionHandlers
{

	private Integer yearId;

	public YearCell()
	{
		super();
		return;
	}

	public YearCell(int yearId, String year)
	{
		super();
		this.yearId = yearId;
		this.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				YearCell.this.fireEvent(new ActionEvent());
				return;
			}
		});
		this.setText(year);
		return;
	}
	public Integer getYearId()
	{
		return this.yearId;
	}
	@Override
	public HandlerRegistration addActionHandler(ActionHandler handler)
	{
		return this.addHandler(handler, ActionEvent.getType());
	}

}
