package com.se.sjtu.edu.arale.browser.index.client.news;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.event.intr.IHasActionHandlers;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;

public class NewsItem extends HyperLink implements IHasActionHandlers
{
	private final News news;
	
	public NewsItem(News news)
	{
		super();
		this.news = news;
		SpanElement e1 = Document.get().createSpanElement();
		e1.setInnerHTML(this.news.getTitle());
		e1.getStyle().setWidth(610, Unit.PX);

		SpanElement e2 = Document.get().createSpanElement();
		String strDate = PaneNews.dtf.format(this.news.getCreatetime());
		e2.setInnerHTML("[" + strDate + "]");
		e2.getStyle().setColor("#787878");
		e2.getStyle().setFloat(Float.RIGHT);
		e2.getStyle().setWidth(120, Unit.PX);
		e2.getStyle().setTextAlign(TextAlign.RIGHT);
		e2.getStyle().setPaddingRight(16.0, Unit.PX);
		//
		//
		if (this.news.isStick())
		{
			ImageElement img = Document.get().createImageElement();
			img.setSrc(GWT.getModuleBaseForStaticFiles() + "Image/new.gif");
			img.getStyle().setFloat(Float.LEFT);
			this.getElement().insertFirst(img);
		}
		this.getElement().insertFirst(e2);
		this.getElement().insertFirst(e1);
		this.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event)
			{
			
				NewsItem.this.fireEvent(new ActionEvent());
				event.preventDefault();
				return;
			}
		});
		return;
	}
	
	@Override
	public void onLoad()
	{
		return;
	}
	public News getNews()
	{
		return this.news;
	}
	@Override
	public HandlerRegistration addActionHandler(ActionHandler handler)
	{
		return this.addHandler(handler, ActionEvent.getType());
	}
}
