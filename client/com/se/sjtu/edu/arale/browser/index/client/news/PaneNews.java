package com.se.sjtu.edu.arale.browser.index.client.news;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.util.client.data.Pager;
import edu.iao.yomi.browser.util.client.data.RequestPageEvent;
import edu.iao.yomi.browser.util.client.data.RequestPageHandler;
import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;
import edu.iao.yomi.vo.resp.ResponseAdapter;

/**
 * 
 * @author lansunlong
 * @since 2013年12月1日
 */
public class PaneNews extends Composite
{
	interface PaneNewsUiBinder extends UiBinder<Widget, PaneNews>
	{
	}
	
	public static PaneNewsUiBinder uiBinder = GWT.create(PaneNewsUiBinder.class);
	public static final DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd");
	@UiField
	VerticalPanel newsPane;
	@UiField
	VerticalPanel noticePane;
	//
	public NewsListPager listPager1 = new NewsListPager();
	public NewsListPager listPager2 = new NewsListPager();
	Pager pager1 = new Pager(7, 5);
	Pager pager2 = new Pager(8, 5);
	private final String lstTag1 = null;
	private final String lstTag2 = null;
	Widget leftPanel;
	
	//
	public PaneNews()
	{

		this.initWidget(PaneNews.uiBinder.createAndBindUi(this));
	}
	
	public PaneNews(Widget w)
	{

		this.initWidget(PaneNews.uiBinder.createAndBindUi(this));
		this.leftPanel = w;
	}
	
	/*
	 * Help Method
	 */
	private void helpShowNewsList(List<News> lst)
	{

		this.helpShowNews(lst, this.newsPane, this.listPager1, this.pager1);
		return;
	}
	
	private void helpShowNoticeList(List<News> lst)
	{

		this.helpShowNews(lst, this.noticePane, this.listPager2, this.pager2);
		return;
	}
	
	private void helpShowNews(List<News> lst, HasWidgets parent, NewsListPager listPager, Pager pager)
	{
		parent.clear();//
		//
		UL ul = new UL();
		for(News n : lst)
		{
			NewsItem link = new NewsItem(n);
			link.addActionHandler(new ActionHandler()
			{
				@Override
				public void onAction(ActionEvent event)
				{

					News ne = ((NewsItem)event.getSource()).getNews();
					((HasWidgets)PaneNews.this.leftPanel).clear();
					((HasWidgets) PaneNews.this.leftPanel).add(new PaneNewsDetail(ne, "首页->新闻", GWT.getHostPageBaseURL() + "Index.html?pid=1"));
					return;
				}
			});
			LI li = new LI();
			li.add(link);
			ul.add(li);
		}
		parent.add(ul);
		parent.add(listPager);
		return;
	}
	
	public void helpRefresh()
	{

		this.pager1.setCurrentPageNumber(this.pager1.getCurrentPageNumber());
		this.pager2.setCurrentPageNumber(this.pager2.getCurrentPageNumber());
	}

	@Override
	public void onLoad()
	{
	
		// this.listPager1.setPager(this.pager1);
		// this.listPager2.setPager(this.pager2);
		//
		this.pager1.addRequestPageHandler(new RequestPageHandler()
		{
			@Override
			public void onRequestItem(final RequestPageEvent event)
			{

				int pageNum = event.getRequestNum();
				int countPerPage = event.getCountPerPage();
				final PageInfo pgInfo = new PageInfo(PaneNews.this.lstTag1, pageNum, countPerPage);
				INewsService.INSTANCE.get().getNewsList(pgInfo, new ResponseAdapter<PageList<News>>()
				{
					@Override
					public void onClear(PageList<News> lst)
					{

						GWT.log("onClear");
						event.doCallback(lst.getCurrentPageNumber(), lst.getTotalCount());
						PaneNews.this.helpShowNewsList(lst);
						return;
					}
					
					@Override
					public void onError(String info)
					{

						GWT.log(info);
						return;
					}
				});
				return;
			}
		});
		this.pager2.addRequestPageHandler(new RequestPageHandler()
		{
			@Override
			public void onRequestItem(final RequestPageEvent event)
			{

				int pageNum = event.getRequestNum();
				int countPerPage = event.getCountPerPage();
				final PageInfo pgInfo = new PageInfo(PaneNews.this.lstTag2, pageNum, countPerPage);
				INewsService.INSTANCE.get().getNoticeList(pgInfo, new ResponseAdapter<PageList<News>>()
				{
					@Override
					public void onClear(PageList<News> lst)
					{

						GWT.log("onClear");
						event.doCallback(lst.getCurrentPageNumber(), lst.getTotalCount());
						PaneNews.this.helpShowNoticeList(lst);
						return;
					}
					
					@Override
					public void onError(String info)
					{

						GWT.log(info);
						return;
					}
				});
				return;
			}
		});
		//
		this.pager1.addPagable(this.listPager1);
		this.pager2.addPagable(this.listPager2);
		this.pager1.setCurrentPageNumber(1);
		this.pager2.setCurrentPageNumber(1);
		this.newsPane.add(this.listPager1);
		this.noticePane.add(this.listPager2);
		//
		return;
	}
}
