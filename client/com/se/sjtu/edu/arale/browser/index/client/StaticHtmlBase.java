package com.se.sjtu.edu.arale.browser.index.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.browser.global.client.GlobalUtil;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.layout.client.Html;
import edu.iao.yomi.browser.layout.client.StaticLayout;
import edu.iao.yomi.browser.util.client.form.label.Label;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class StaticHtmlBase extends Composite
{
	interface StaticHtmlBaseUiBinder extends UiBinder<Widget, StaticHtmlBase>
	{
	}
	
	private static StaticHtmlBaseUiBinder uiBinder = GWT.create(StaticHtmlBaseUiBinder.class);
	@UiField
	Label lblTitle;
	@UiField
	Html html;
	@UiField
	StaticLayout base;
	Integer newsId;
	String info;
	
	public StaticHtmlBase(Integer newsId, String info)
	{

		this.initWidget(StaticHtmlBase.uiBinder.createAndBindUi(this));
		this.newsId = newsId;
		this.info = info;
		return;
	}
	
	@Override
	public void onLoad()
	{

		INewsService.INSTANCE.get().getStaticNews(this.newsId, new ResponseAdapter<News>()
		{
			@Override
			public void onClear(News content)
			{

				StaticHtmlBase.this.helpSetContent(content);
				return;
			}
			
			@Override
			public void onError(String info)
			{

				super.onError(info);
			}
		});
		return;
	}
	
	private void helpSetContent(News news)
	{
	
		this.lblTitle.setText(this.info);
		String str = news.getContent();
		str = GlobalUtil.htmlSpecialCharsReverse(str);
		//
		this.html.setHTML("<div class=\"descontent\">" + str + "</div>");
		// int height = this.html.getElement().getClientHeight();
		// this.base.setHeight(height + "px");
		return;
	}
}
