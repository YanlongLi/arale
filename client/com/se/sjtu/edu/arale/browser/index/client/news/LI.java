package com.se.sjtu.edu.arale.browser.index.client.news;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public class LI extends ComplexPanel implements HasHTML, InsertPanel.ForIsWidget
{
	public LI()
	{
		this.setElement(Document.get().createLIElement());
	}
	
	/*
	 * HasHtml
	 */
	@Override
	public String getHTML()
	{
	
		return this.getElement().getInnerHTML();
	}
	
	@Override
	public String getText()
	{
	
		return this.getElement().getInnerText();
	}
	
	@Override
	public void setHTML(String html)
	{
	
		this.getElement().setInnerHTML(html);
		return;
	}
	
	@Override
	public void setText(String text)
	{
	
		this.getElement().setInnerText(text);
		return;
	}
	
	@Override
	public void add(Widget w)
	{
	
		this.add(w, this.getElement());
	}
	
	@Override
	public void clear()
	{
	
		// Remove all existing child nodes.
		Node child = this.getElement().getFirstChild();
		while(child != null)
		{
			this.getElement().removeChild(child);
			child = this.getElement().getFirstChild();
		}

	}
	
	@Override
	public void insert(IsWidget w, int beforeIndex)
	{
	
		this.insert(Widget.asWidgetOrNull(w), beforeIndex);
	}
	
	@Override
	public void insert(Widget w, int beforeIndex)
	{
	
		this.insert(w, this.getElement(), beforeIndex, true);
	}
}
