package com.se.sjtu.edu.arale.browser.index.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

import edu.iao.yomi.browser.util.client.TextUtil;

public class Index implements EntryPoint
{
	@Override
	public void onModuleLoad()
	{

		//
		RootPanel root = RootPanel.get();
		//
		String pid = Window.Location.getParameter("pid");
		if (TextUtil.isNullOrZero(pid))
		{
			pid = "10";
		}
		Integer id = null;
		try
		{
			id = Integer.parseInt(pid);
		}
		catch (Exception e)
		{
			id = 10;
		}
		root.add(new BasePanel(id));

	}
}
