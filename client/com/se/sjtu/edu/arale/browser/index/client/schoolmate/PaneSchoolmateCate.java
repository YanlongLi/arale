package com.se.sjtu.edu.arale.browser.index.client.schoolmate;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.service.client.ISchoolmatesService;
import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.browser.layout.client.FloatBreak;
import edu.iao.yomi.browser.layout.client.LeftFloatLayout;
import edu.iao.yomi.browser.layout.client.StaticLayout;
import edu.iao.yomi.browser.util.client.event.ActionEvent;
import edu.iao.yomi.browser.util.client.event.ActionHandler;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;
import edu.iao.yomi.vo.resp.ResponseAdapter;

public class PaneSchoolmateCate extends Composite implements ActionHandler
{
	public static DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy年MM月");
	interface PaneSchoolmateCateUiBinder extends UiBinder<Widget, PaneSchoolmateCate>
	{
	}

	private static PaneSchoolmateCateUiBinder uiBinder = GWT.create(PaneSchoolmateCateUiBinder.class);
	@UiField
	DeckPanel deck;
	@UiField
	StaticLayout yearPane;
	@UiField
	HyperLink btnTitle;

	/*
	 * 
	 */
	public PaneSchoolmateCate()
	{
		initWidget(uiBinder.createAndBindUi(this));
		return;
	}

	@Override
	protected void onLoad()
	{
		ISchoolmatesService.INSTANCE.get().getSchoolmatesList(new ResponseAdapter<List<Schoolmates>>()
		{
			@Override
			public void onClear(List<Schoolmates> list)
			{
				PaneSchoolmateCate.this.helpAddYearList(list);
				return;
			}

			@Override
			public void onError(String err)
			{
				return;
			}
		});
		return;
	}

	private void helpSwitchToYearList()
	{
		this.deck.showWidget(0);
		return;
	}

	private void helpSitchToNameList(Schoolmates sm)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<div style=\"width:100%;text-align:center;margin:10px 0px;\">" + PaneSchoolmateCate.dtf.format(sm.getGraduateTime()) + "毕业学生</div>");
		sb.append("<table width=\"95%\" style=\"margin:0px auto;font-size:15px;line-height:22px;\">");
		String[] names = sm.getNamelist().split("&&&&&");
		sb.append("<tr>");
		sb.append("<td>学号</td>");
		sb.append("<td>姓名</td>");
		sb.append("<td>指导教师</td>");
		sb.append("<td>论文题目</td>");
		sb.append("</tr>");
		for (int i = 0; i < names.length; i++)
		{
			sb.append("<tr>");
			sb.append("<td>" + names[i++] + "</td>");
			sb.append("<td>" + names[i++] + "</td>");
			sb.append("<td>" + names[i++] + "</td>");
			sb.append("<td>" + names[i] + "</td>");
			sb.append("</tr>");
		}
		sb.append("</table>");
		HTMLPanel html = new HTMLPanel(sb.toString());
		html.getElement().getStyle().setFontSize(14, Unit.PX);
		if (this.deck.getWidgetCount() > 1)
		{
			this.deck.remove(1);
		}
		this.deck.add(html);
		this.deck.showWidget(1);
		return;
	}

	/*
	 * 
	 */
	private void helpAddYearList(List<Schoolmates> list)
	{
		this.yearPane.clear();
		for (Schoolmates sm : list)
		{
			LeftFloatLayout lf = new LeftFloatLayout();
			lf.setStyleName("sm_year");
			YearCell yc = new YearCell(sm.getId(), PaneSchoolmateCate.dtf.format(sm.getGraduateTime()));
			yc.addActionHandler(this);
			lf.add(yc);
			this.yearPane.add(lf);
		}
		this.yearPane.add(new FloatBreak());
		this.helpSwitchToYearList();
		return;
	}

	@Override
	public void onAction(ActionEvent event)
	{
		YearCell yc = (YearCell) event.getSource();
		int id = yc.getYearId();
		ISchoolmatesService.INSTANCE.get().getNameList(id, new ResponseAdapter<Schoolmates>()
		{
			@Override
			public void onClear(Schoolmates sm)
			{
				PaneSchoolmateCate.this.helpSitchToNameList(sm);
				return;
			}

			@Override
			public void onError(String err)
			{
				return;
			}
		});
	}

	@UiHandler("btnTitle")
	void onBtnTitleClick(ClickEvent event)
	{
		this.helpSwitchToYearList();
		return;
	}
}
