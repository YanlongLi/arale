/**
 * PaneNewsDetail.java
 */
package com.se.sjtu.edu.arale.browser.index.client.news;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.se.sjtu.edu.arale.browser.global.client.GlobalUtil;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.browser.layout.client.Html;
import edu.iao.yomi.browser.util.client.form.button.HyperLink;

/**
 * @author lansunlong
 * @since 2013年12月3日
 */
public class PaneNewsDetail extends Composite
{
	private static PaneNewsDetailUiBinder uiBinder = GWT.create(PaneNewsDetailUiBinder.class);
	
	interface PaneNewsDetailUiBinder extends UiBinder<Widget, PaneNewsDetail>
	{
	}
	
	@UiField
	HyperLink lblTitle;
	@UiField
	Html txtContent;
	//
	private News news;
	private String info;
	public PaneNewsDetail()
	{
		this.initWidget(PaneNewsDetail.uiBinder.createAndBindUi(this));
		return;
	}
	
	public PaneNewsDetail(News news, String info, String url)
	{
		this.initWidget(PaneNewsDetail.uiBinder.createAndBindUi(this));
		this.news = news;
		this.info = info;
		this.lblTitle.setUrl(url);
		return;
	}
	
	@Override
	public void onLoad()
	{
		this.helpShowNews();
		return;
	}
	
	private void helpShowNews()
	{
		// TODO
		// this.lblTitle.setText(this.news.getTitle());
		this.lblTitle.setText(this.info);
		String str = this.news.getContent();
		str = GlobalUtil.htmlSpecialCharsReverse(str);
		this.txtContent.setHTML("<div class=\"descontent\">" + str + "</div>");
		return;
	}
	// @Override
	// public void onLoad()
	// {
	// if(this.newsId == null)
	// {
	// throw new NullPointerException("newsId == null");
	// }
	// INewsService.INSTANCE.get().getNews(newsId, new ResponseAdapter<News>()
	// {
	// @Override
	// public void onClear(News content)
	// {
	// PaneNewsDetail.this.helpShowNews(content);
	// return;
	// }
	//
	// @Override
	// public void onError(String info)
	// {
	// super.onError(info);
	// }
	// });
	// return;
	// }
}
