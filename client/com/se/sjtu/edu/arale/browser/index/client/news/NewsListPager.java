package com.se.sjtu.edu.arale.browser.index.client.news;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

import edu.iao.yomi.browser.layout.client.RightFloatLayout;
import edu.iao.yomi.browser.util.client.data.Pager;
import edu.iao.yomi.browser.util.client.data.intr.IPagableWidget;
import edu.iao.yomi.browser.util.client.form.button.Button;
import edu.iao.yomi.browser.util.client.form.button.InlineHyperLink;

/**
 * 
 * @author lansunlong
 * @since 2013年12月1日
 */
public class NewsListPager extends Composite implements IPagableWidget
{
	interface NewsListPagerUiBinder extends UiBinder<Widget, NewsListPager>
	{
	}
	
	private static NewsListPagerUiBinder uiBinder = GWT.create(NewsListPagerUiBinder.class);
	@UiField
	Button btnLast;
	@UiField
	Button btnFirst;
	@UiField
	Button btnPrev;
	@UiField
	Button btnNext;
	@UiField
	RightFloatLayout numContainer;
	//
	private Pager pager;
	
	public NewsListPager()
	{
		this.initWidget(NewsListPager.uiBinder.createAndBindUi(this));
	}
	
	//
	// IPageble 接口实现
	//
	@Override
	public void setPager(Pager pager)
	{
		this.pager = pager;
		return;
	}
	
	@Override
	public void init()
	{
		this.btnFirst.setEnabled(false);
		this.btnLast.setEnabled(false);
		this.btnPrev.setEnabled(false);
		this.btnNext.setEnabled(false);
		this.numContainer.clear();
		return;
	}
	
	@Override
	public void render(int iCrtPageNum, int iTotalCount, boolean bFirstEnabled, boolean bPreviousEnabled, List<Integer> leftPartNumLst, List<Integer> rightPartNumLst, boolean bNextEnabled, boolean bLastEnabled)
	{
	
		GWT.log("render:" + iCrtPageNum + " " + iTotalCount + " " + leftPartNumLst.size() + " " + rightPartNumLst.size());
		this.btnFirst.setEnabled(bFirstEnabled);
		this.btnPrev.setEnabled(bPreviousEnabled);
		this.btnNext.setEnabled(bNextEnabled);
		this.btnLast.setEnabled(bLastEnabled);
		this.numContainer.clear();
		for(Integer i : leftPartNumLst)
		{
			InlineHyperLink link = new InlineHyperLink();
			link.setPadding("0px 5px");
			link.setText(i.toString());
			link.addClickHandler(new ClickHandler()
			{
				@Override
				public void onClick(ClickEvent event)
				{
					int pageNum = Integer.parseInt(((HasText)event.getSource()).getText());
					GWT.log("set page:" + pageNum);
					NewsListPager.this.pager.setCurrentPageNumber(pageNum);
					return;
				}
			});
			this.numContainer.add(link);
		}
		{
			InlineHyperLink link = new InlineHyperLink();
			link.setPadding("0px 5px");
			link.setText(Integer.toString(iCrtPageNum));
			link.setEnabled(false);
			this.numContainer.add(link);
		}
		for(Integer i : rightPartNumLst)
		{
			InlineHyperLink link = new InlineHyperLink();
			link.setPadding("0px 5px");
			link.setText(i.toString());
			link.addClickHandler(new ClickHandler()
			{
				@Override
				public void onClick(ClickEvent event)
				{
					int pageNum = Integer.parseInt(((HasText)event.getSource()).getText());
					NewsListPager.this.pager.setCurrentPageNumber(pageNum);
					return;
				}
			});
			this.numContainer.add(link);
		}
		return;
	}
	
	//
	// Event Handler
	//
	@UiHandler("btnNext")
	void onBtnNextClick(ClickEvent event)
	{
		this.pager.setCurrentPageNumber(this.pager.getCurrentPageNumber() + 1);
		return;
	}
	
	@UiHandler("btnPrev")
	void onBtnPrevClick(ClickEvent event)
	{
		this.pager.setCurrentPageNumber(this.pager.getCurrentPageNumber() - 1);
		return;
	}
	
	@UiHandler("btnFirst")
	void onBtnFirstClick(ClickEvent event)
	{
		this.pager.setCurrentPageNumber(1);
		return;
	}
	
	@UiHandler("btnLast")
	void onBtnLastClick(ClickEvent event)
	{
		this.pager.setCurrentPageNumber(this.pager.getTotalCount());
		return;
	}
}
