package com.se.sjtu.edu.arale.session;

import javax.servlet.http.HttpSession;

import com.se.sjtu.edu.arale.config.Arale;

public class ViewCountSession
{
	private final HttpSession session;
	
	public ViewCountSession(HttpSession session)
	{
		this.session = session;
	}
	
	public void setViewCount(Integer count)
	{
		this.session.setAttribute(Arale.SESSION_VIEW_COUNT, count);
		return;
	}
	
	public Integer getViewCount()
	{
		return (Integer)this.session.getAttribute(Arale.SESSION_VIEW_COUNT);
	}
}
