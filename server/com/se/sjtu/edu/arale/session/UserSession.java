/**
 * UserSession.java
 */
package com.se.sjtu.edu.arale.session;

import javax.servlet.http.HttpSession;

import com.se.sjtu.edu.arale.dao.DaoFactory;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.service.session.AUserSession;

/**
 * @author lansunlong
 * @since 2013年12月2日
 */
public class UserSession extends AUserSession<User>
{
	public UserSession(HttpSession session)
	{
		super(session);
	}
	
	@Override
	public int getUserVoLifeCount()
	{
		return 20;
	}
	
	@Override
	protected User findUserVo(Integer id)
	{
		User user = null;
		try
		{
			user = DaoFactory.createUserDao().findById(id);
		}
		catch(ExecuteException | ConnectionException e)
		{
			e.printStackTrace();
		}
		return user;
	}
}
