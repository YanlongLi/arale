package com.se.sjtu.edu.arale.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.se.sjtu.edu.arale.dao.intr.ISchoolmatesDao;
import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.dao.AStandardDaoImpl;
import edu.iao.yomi.dao.DbConnector;
import edu.iao.yomi.dao.avsql.intr.IAvResultForSelect;
import edu.iao.yomi.dao.exception.AvSyntaxException;
import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;

public class SchoolmatesDaoImpl extends AStandardDaoImpl<Schoolmates> implements ISchoolmatesDao
{

	public SchoolmatesDaoImpl(DbConnector dbConnector) throws ConnectionException
	{
		super(dbConnector);
		return;
	}

	@Override
	public List<Schoolmates> findListDesc() throws ExecuteException, AvSyntaxException
	{
		List<Schoolmates> lst = new ArrayList<Schoolmates>();
		IAvResultForSelect res = this.createAvSql().select(Schoolmates.ID + "," + Schoolmates.GRADUATETIME + "," + Schoolmates.NAMELIST).from(Schoolmates.TABLE)
				.orderBy(Schoolmates.GRADUATETIME + " DESC").assign().doSelect();
		System.out.println("begin" + res.getSelectedRowAmount());
		while (res.hasNext())
		{
			Schoolmates sm = new Schoolmates();
			res.fetchVo(sm);
			lst.add(sm);
		}
		System.out.println("End");
		return lst;
	}

}
