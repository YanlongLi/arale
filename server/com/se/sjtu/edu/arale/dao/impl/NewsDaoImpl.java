/**
 * 
 */
package com.se.sjtu.edu.arale.dao.impl;

import com.se.sjtu.edu.arale.dao.intr.INewsDao;
import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.dao.AStandardDaoImpl;
import edu.iao.yomi.dao.DbConnector;
import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.dao.session.PageSession;
import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public class NewsDaoImpl extends AStandardDaoImpl<News> implements INewsDao
{
	public NewsDaoImpl(DbConnector dbConnector) throws ConnectionException
	{
		super(dbConnector);
	}
	
	@Override
	public PageList<News> getNewsPageListByType(PageSession<News> pgSession, PageInfo pgInfo, final int type, final boolean isAdmin) throws ExecuteException
	{
		StringBuffer sb = new StringBuffer();
		if(type == News.TYPE_NEWS)
		{
			sb.append("" +News.TYPE + " = " +News.TYPE_NEWS);
		}
		else
		{
			sb.append(" ( " + News.TYPE + " = " + News.TYPE_NOTICE + " OR " + News.TYPE + " = " + News.TYPE_XNOTICE + " ) ");
		}
		if(isAdmin)
		{
			return this.findPage(pgSession, pgInfo, " " + sb.toString(), " " + News.PUBLISH_TIME + " DESC ");
		}
		return this.findPage(pgSession, pgInfo, " " + sb.toString() + " AND " + News.STATUS + " = " + News.STATUS_PUBLISHED, " " + News.PUBLISH_TIME + " DESC ");
		// try
		// {
		// return pgSession.getPageList(pgInfo.getListTag(), pgInfo.getPageNum(), pgInfo.getCountPerPage(), new APageRequester<News>(null, null)
		// {
		// private IAvResultForSelect r = null;
		//
		// @Override
		// public int queryTotalCount() throws AvSyntaxException
		// {
		// return r.getSelectedRowAmount();
		// }
		//
		// @Override
		// public IAvResultForSelect query() throws AvSyntaxException
		// {
//					//@formatter:off
//					if(isAdmin)
//					{
//						return this.r = NewsDaoImpl.this.createAvSql().
//							select(" * ").from(News.TABLE).where(News.TYPE + " = ?" ).assign().setInt(1, type).doSelect();
//					}
//					else
//					{
//						return this.r = NewsDaoImpl.this.createAvSql().
//								select(" * ").from(News.TABLE).where(News.TYPE + " = ? AND " + News.STATUS + " =  ? " ).assign().setInt(1, type).setInt(2, News.STATUS_PUBLISHED).doSelect();						
//					}
//					//@formatter:on
		// }
		//
		// @Override
		// public News fetch(IAvResultForSelect r) throws AvSyntaxException
		// {
		// final News tmp = new News();
		// r.fetchVo(tmp);
		// return tmp;
		// }
		// });
		// }
		// catch(AvSyntaxException e)
		// {
		// throw new ExecuteException(e.getMessage());
		// }
	}
}
