/**
 * 
 */
package com.se.sjtu.edu.arale.dao.impl;

import com.se.sjtu.edu.arale.dao.intr.IUserDao;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.dao.AStandardDaoImpl;
import edu.iao.yomi.dao.DbConnector;
import edu.iao.yomi.dao.exception.ConnectionException;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public class UserDaoImpl extends AStandardDaoImpl<User> implements IUserDao
{
	public UserDaoImpl(DbConnector dbConnector) throws ConnectionException
	{
		super(dbConnector);
	}
}
