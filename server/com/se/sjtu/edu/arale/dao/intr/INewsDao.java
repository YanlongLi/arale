package com.se.sjtu.edu.arale.dao.intr;

import com.se.sjtu.edu.arale.vo.client.News;

import edu.iao.yomi.dao.IStandardDao;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.dao.session.PageSession;
import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;

/**
 * 
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface INewsDao extends IStandardDao<News>
{
	public PageList<News> getNewsPageListByType(PageSession<News> pgSession, PageInfo pgInfo, int type, boolean isAdmin) throws ExecuteException;
}
