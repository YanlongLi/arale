package com.se.sjtu.edu.arale.dao.intr;

import java.util.List;

import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.dao.IStandardDao;
import edu.iao.yomi.dao.exception.AvSyntaxException;
import edu.iao.yomi.dao.exception.ExecuteException;

public interface ISchoolmatesDao extends IStandardDao<Schoolmates>
{
	public List<Schoolmates> findListDesc() throws ExecuteException, AvSyntaxException;

}
