package com.se.sjtu.edu.arale.dao.intr;

import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.dao.IStandardDao;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public interface IUserDao extends IStandardDao<User>
{
}
