package com.se.sjtu.edu.arale.dao;

import com.se.sjtu.edu.arale.config.Arale;
import com.se.sjtu.edu.arale.dao.impl.NewsDaoImpl;
import com.se.sjtu.edu.arale.dao.impl.SchoolmatesDaoImpl;
import com.se.sjtu.edu.arale.dao.impl.UserDaoImpl;
import com.se.sjtu.edu.arale.dao.intr.INewsDao;
import com.se.sjtu.edu.arale.dao.intr.ISchoolmatesDao;
import com.se.sjtu.edu.arale.dao.intr.IUserDao;

import edu.iao.yomi.dao.exception.ConnectionException;

public class DaoFactory
{
	public static synchronized IUserDao createUserDao() throws ConnectionException
	{
		return new UserDaoImpl(Arale.DB_CONNECTOR_FOR_ARALE);
	}
	public static synchronized INewsDao createNewsDao() throws ConnectionException
	{
		return new NewsDaoImpl(Arale.DB_CONNECTOR_FOR_ARALE);
	}

	public static synchronized ISchoolmatesDao createSchoolmatesDao() throws ConnectionException
	{
		return new SchoolmatesDaoImpl(Arale.DB_CONNECTOR_FOR_ARALE);
	}
}
