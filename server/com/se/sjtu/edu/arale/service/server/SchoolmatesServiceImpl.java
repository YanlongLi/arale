package com.se.sjtu.edu.arale.service.server;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.se.sjtu.edu.arale.config.Arale;
import com.se.sjtu.edu.arale.dao.DaoFactory;
import com.se.sjtu.edu.arale.service.client.ISchoolmatesService;
import com.se.sjtu.edu.arale.vo.client.Schoolmates;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.config.Config;
import edu.iao.yomi.dao.exception.AvSyntaxException;
import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.service.Service;
import edu.iao.yomi.service.util.FileUtil;
import edu.iao.yomi.vo.resp.Response;

public class SchoolmatesServiceImpl extends Service implements ISchoolmatesService
{

	private static final long serialVersionUID = 1L;

	@Override
	public Response<List<Schoolmates>> getSchoolmatesList()
	{
		Response<List<Schoolmates>> resp = new Response<List<Schoolmates>>();
		List<Schoolmates> lst = null;
		try
		{
			lst = DaoFactory.createSchoolmatesDao().findListDesc();
		}
		catch (ExecuteException | ConnectionException | AvSyntaxException e)
		{
			resp.setErrorInfo("服务器错误，重试");
			Arale.PrintDebugInfo("ERROR：查询错误," + e.getMessage());
			return resp;
		}
		if (lst == null || lst.isEmpty())
		{
			resp.setErrorInfo("没有数据");
			return resp;
		}
		resp.setContent(lst);
		return resp;
	}

	@Override
	public Response<Schoolmates> getNameList(Integer yearId)
	{
		Response<Schoolmates> resp = new Response<>();
		if (yearId == null)
		{
			throw new NullPointerException("yearId == null");
		}
		Schoolmates sm = null;
		try
		{
			sm = DaoFactory.createSchoolmatesDao().findById(yearId);
		}
		catch (ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("服务器错误，重试");
			Arale.PrintDebugInfo("ERROR：查询错误," + e.getMessage());
			return resp;
		}
		if (sm == null)
		{
			resp.setErrorInfo("查询失败");
			return resp;
		}
		resp.setContent(sm);
		return resp;
	}

	@Override
	public Response<String> addSchoolmatesFromExcel(String filePath)
	{
		Response<String> resp = new Response<String>();
		if (filePath == null)
		{
			throw new NullPointerException("filePath == null");
		}
		filePath = this.getServletContext().getRealPath(Config.DIRECTORY_FOR_TEMPORARY + "/" + filePath);
		String forbidden = null;
		forbidden = this.validateLimits();
		if (forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		Schoolmates sm = null;
		try
		{
			sm = SchoolmatesServiceImpl.helpMakeSm(filePath);
		}
		catch (Exception e)
		{
			resp.setErrorInfo(e.getMessage());
			return resp;
		}
		Integer id = null;
		try
		{
			id = DaoFactory.createSchoolmatesDao().add(sm);
		}
		catch (ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("添加校友目录失败" + e.getMessage());
			return resp;
		}
		if (id == null || id.intValue() == 0)
		{
			resp.setErrorInfo("添加校友目录失败");
			return resp;
		}
		resp.setContent("添加校友目录成功");
		return resp;
	}

	private static Schoolmates helpMakeSm(String filePath) throws Exception
	{
		Schoolmates sm = new Schoolmates();
		//
		Workbook book = Workbook.getWorkbook(new File(filePath));
		Sheet sheet = book.getSheet(0);
		if (sheet == null)
		{
			throw new Exception("Excel文件不存在或无法打开");
		}
		int iCol = sheet.getColumns();
		int iRow = sheet.getRows();
		Arale.PrintDebugInfo("iRow:" + iRow);
		if (iCol == 0 || iRow == 0)
		{
			throw new Exception("文件中没有有效内容");
		}
		String strDate = sheet.getCell(0, 0).getContents();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM");
		Date date = sdf.parse(strDate);
		sm.setGraduateTime(date);
		StringBuffer sb = new StringBuffer();
		//
		for (int row = 1; row < iRow; row++)
		{
			String sno = sheet.getCell(0, row).getContents();
			String name = sheet.getCell(1, row).getContents();
			String advisor = sheet.getCell(2, row).getContents();
			String paper = sheet.getCell(3, row).getContents();
			if (TextUtil.isNullOrZero(sno))
			{
				break;
			}
			sb.append(sno).append("&&&&&").append(name).append("&&&&&").append(advisor).append("&&&&&").append(paper).append("&&&&&");
		}
		sm.setNamelist(sb.toString());
		sm.setId(null);
		return sm;
	}

	public void uploadExcel(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/html;charset=utf-8");
		String forbidden = null;
		forbidden = this.validateLimits();
		if (forbidden != null)
		{
			resp.getWriter().print("error : 没有上传权限");
			return;
		}
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);

		List<?> items = null;
		try
		{
			items = upload.parseRequest(req);
		}
		catch (FileUploadException e)
		{
			throw new ServletException(e);
		}
		for (Object obj : items)
		{
			FileItem item = (FileItem) obj;
			if (item.isFormField() == false)
			{
				String realTmpPath = this.getServletContext().getRealPath(Config.DIRECTORY_FOR_TEMPORARY);
				String suffix = null;
				String tmpFileName = FileUtil.makeRandomFileName(realTmpPath, suffix = FileUtil.getFileSuffix(item.getName()));
				//
				if (!"xls".equals(suffix))
				{
					resp.getWriter().print("error : 不支持的文件类型");
					return;
				}
				String tmpFilePath = realTmpPath + "/" + tmpFileName;
				File tmpFile = new File(tmpFilePath);
				try
				{
					item.write(tmpFile);
				}
				catch (Exception e)
				{
					resp.getWriter().print("ERROR : " + e.getMessage());
					return;
				}
				resp.getWriter().print("success : " + tmpFileName);
				return;
			}
			resp.getWriter().print("error : 没有上传任何文件");
		}

		return;
	}
}
