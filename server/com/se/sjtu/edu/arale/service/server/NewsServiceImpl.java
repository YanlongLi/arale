/**
 * 
 */
package com.se.sjtu.edu.arale.service.server;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.se.sjtu.edu.arale.config.Arale;
import com.se.sjtu.edu.arale.dao.DaoFactory;
import com.se.sjtu.edu.arale.service.client.INewsService;
import com.se.sjtu.edu.arale.service.validator.LoginValidator;
import com.se.sjtu.edu.arale.session.UserSession;
import com.se.sjtu.edu.arale.vo.client.News;
import com.se.sjtu.edu.arale.vo.client.NewsComparator;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.dao.Dao;
import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.service.Method;
import edu.iao.yomi.service.Service;
import edu.iao.yomi.util.PageInfo;
import edu.iao.yomi.util.PageList;
import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public class NewsServiceImpl extends Service implements INewsService
{
	private static final long serialVersionUID = 1L;
	
	@Method(validator = LoginValidator.class)
	@Override
	public Response<Integer> addNews(News news)
	{
		return this.helpAddOrUpdateNews(null, news);
	}
	
	@Override
	public Response<News> getNews(Integer newsId)
	{
		User user = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUser(false);
		if(user == null)
		{
			Arale.PrintDebugInfo("INFO：getNews by anonymous");
			return this.helpGetNews(newsId, false);
		}
		else
		{
			Arale.PrintDebugInfo("INFO：getNews by admin " + user.getUsername());
			return this.helpGetNews(newsId, true);
		}
	}
	
	@Override
	public Response<Integer> updateNews(Integer newsId, News news)
	{
		return this.helpAddOrUpdateNews(newsId, news);
	}
	
	@Method(validator = LoginValidator.class)
	@Override
	public Response<String> removeNews(Integer newsId)
	{
		if(newsId == null)
		{
			throw new NullPointerException("newsId == null");
		}
		Response<String> resp = new Response<String>();
		if(newsId < 11)
		{
			resp.setErrorInfo("静态页面不能删除");
			return resp;
		}
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		int i = 0;
		try
		{
			i = DaoFactory.createNewsDao().remove(newsId);
			if(i < 1)
			{
				resp.setErrorInfo("删除失败");
				return resp;
			}
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("服务器错误，重试");
			Arale.PrintDebugInfo("ERROR：删除错误," + e.getMessage());
			return resp;
		}
		resp.setContent("删除成功");
		return resp;
	}
	
	@Override
	public Response<PageList<News>> getNewsList(PageInfo pgInfo)
	{
		User user = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUser(false);
		boolean isAdmin = (user != null) ? true : false;
		return this.helpGetNewsList(News.TYPE_NEWS, pgInfo, isAdmin);
	}
	
	@Override
	public Response<PageList<News>> getNoticeList(PageInfo pgInfo)
	{
		User user = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUser(false);
		boolean isAdmin = (user != null) ? true : false;
		return this.helpGetNewsList(News.TYPE_NOTICE, pgInfo, isAdmin);
	}
	
	@Method(validator = LoginValidator.class)
	@Override
	public Response<String> publishNews(Integer newsId)
	{
		if(newsId == null)
		{
			throw new NullPointerException("newsId == null");
		}
		Response<String> resp = new Response<String>();
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		News news = null;
		try
		{
			news = DaoFactory.createNewsDao().findById(newsId);
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("查找失败");
			Arale.PrintDebugInfo("ERROR：发布新闻查找失败" + e.getMessage());
			return resp;
		}
		if(news == null)
		{
			resp.setErrorInfo("要发布的新闻不存在");
			Arale.PrintDebugInfo("ERROR：发布新闻不存在，ID：" + newsId);
			return resp;
		}
		if(news.getStatus() == News.STATUS_PUBLISHED)
		{
			resp.setErrorInfo("新闻已经发布，无需重复发布");
			Arale.PrintDebugInfo("ERROR：尝试发布已经发布的新闻，ID：" + newsId);
			return resp;
		}
		news.setId(null);
		news.setStatus(News.STATUS_PUBLISHED);
		forbidden = Dao.validateVoForUpdate(news);
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		forbidden = Dao.validateVoForUpdate(news);
		if(forbidden != null)
		{
			resp.setErrorInfo("数据有误");
			Arale.PrintDebugInfo("ERROR：发布新闻Vo字段有误，" + forbidden);
			return resp;
		}
		int i = 0;
		try
		{
			i = DaoFactory.createNewsDao().update(newsId, news);
			if(i < 1)
			{
				resp.setErrorInfo("更新数据库失败");
				Arale.PrintDebugInfo("ERROR：发布新闻更新状态失败，ID：" + newsId);
				return resp;
			}
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("更新数据库失败" + e.getMessage());
			Arale.PrintDebugInfo("ERROR：发布新闻更新状态异常，ID：" + newsId + e.getMessage());
			return resp;
		}
		//
		resp.setContent("发布成功");
		return resp;
	}
	
	/**
	 * Help Methord
	 */
	@Method(validator = LoginValidator.class)
	private Response<Integer> helpAddOrUpdateNews(Integer newsId, News news)
	{
		Response<Integer> resp = new Response<Integer>();
		//
		// check parameter
		if(news == null)
		{
			new NullPointerException("new  == null");
		}
		news.setContent(news.getContent().replaceAll("'", "‘"));
		news.setId(null);
		boolean bIsAdd = (newsId == null);
		//
		// check limits
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		// modify data
		if(news.getStatus() == null || news.getStatus() != News.STATUS_PUBLISHED)
		{
			Arale.PrintDebugInfo("set news status to draft");
			news.setStatus(News.STATUS_DRAFT);
		}
		Date date = new Date();
		if(bIsAdd)
		{
			news.setCreatetime(date);
			news.setPublishtime(date);
		}
		news.setUpdatetime(date);
		if (news.getSticktime() == null)
		{
			news.setSticktime(date);
		}
		Integer authorId = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUserId();
		if(authorId == null)
		{
			Arale.PrintDebugInfo("添加新闻，没有用户登录");
			resp.setErrorInfo("您没有登录，请重新登录");
			return resp;
		}
		news.setAuthorid(authorId);
		if(bIsAdd)
		{
			forbidden = Dao.validateVoForInsert(news);
		}
		else
		{
			forbidden = Dao.validateVoForUpdate(news);
		}
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		// Arale.PrintDebugInfo("add or insert:" + news.getTitle());
		//
		//
		int i = 0;
		if(bIsAdd)
		{
			try
			{
				i = DaoFactory.createNewsDao().add(news);
			}
			catch(ExecuteException | ConnectionException e)
			{
				resp.setErrorInfo("添加新闻失败");
				Arale.PrintDebugInfo("ERROR：添加新闻异常" + e.getMessage());
				return resp;
			}
			if(i < 1)
			{
				resp.setErrorInfo("添加新闻失败");
				Arale.PrintDebugInfo("ERROR：添加新闻异常");
				return resp;
			}
			newsId = i;
		}
		else
		{
			try
			{
				i = DaoFactory.createNewsDao().update(newsId, news);
			}
			catch(ExecuteException | ConnectionException e)
			{
				resp.setErrorInfo("更新新闻失败");
				Arale.PrintDebugInfo("ERROR：更新新闻异常" + e.getMessage());
				return resp;
			}
			if(i < 1)
			{
				resp.setErrorInfo("更新新闻失败");
				Arale.PrintDebugInfo("ERROR：更新新闻失败");
				return resp;
			}
		}
		//
		//
		resp.setContent(newsId);
		return resp;
	}
	
	@Method(validator = LoginValidator.class)
	@Override
	public Response<List<News>> getAllNewsByAdmin()
	{
		Response<List<News>> resp = new Response<List<News>>();
		//
		// validate limits
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		List<News> lst = null;
		User user = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUser(false);
		try
		{
			lst = DaoFactory.createNewsDao().findList(News.AUTHORID + " = " + user.getId(), News.CREATE_TIME + " DESC ," + News.PUBLISH_TIME + " DESC ");
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("查找失败");
			Arale.PrintDebugInfo("ERROR：查找列表失败，" + e.getMessage());
			return resp;
		}
		//
		// set author
		try
		{
			for(News news : lst)
			{
				User author = new User();
				author = DaoFactory.createUserDao().findById(news.getAuthorid());
				if(author == null)
				{
					Arale.PrintDebugInfo("ERROR：查找的新闻的作者不存在, newsId" + news.getId() + ",authorId:" + news.getAuthorid());
					// resp.setErrorInfo("");
					// return resp;
					continue;
				}
				// Arale.PrintDebugInfo("get news list:" + news.getTitle());
				news.setAuthor(author);
			}
		}
		catch(ExecuteException | ConnectionException e)
		{
			Arale.PrintDebugInfo("ERROR：查找新闻列表异常" + e.getMessage());
		}
		//
		// return
		resp.setContent(lst);
		return resp;
	}
	
	private Response<News> helpGetNews(Integer newsId, boolean isAdmin)
	{
		Response<News> resp = new Response<News>();
		//
		// check parameters
		if(newsId == null)
		{
			throw new NullPointerException("newsId == null");
		}
		//
		// check limits
		String forbidden = null;
		forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		// get news
		News news = null;
		try
		{
			news = DaoFactory.createNewsDao().findById(newsId);
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo(e.getMessage());
			return resp;
		}
		if(news == null)
		{
			resp.setErrorInfo("查找的新闻不存在");
			Arale.PrintDebugInfo("ERROR：查找的新闻不存在, id" + newsId);
			return resp;
		}
		//
		// check if the news is draft
		if(!isAdmin && news.getStatus() != News.STATUS_PUBLISHED)
		{
			resp.setErrorInfo("新闻不存在");
			Arale.PrintDebugInfo("ERROR：权限错误，企图查看未发布的新闻");
			return resp;
		}
		//
		// get author
		User author = null;
		try
		{
			author = DaoFactory.createUserDao().findById(news.getAuthorid());
			if(author == null)
			{
				resp.setErrorInfo("查找的新闻的无作者");
				Arale.PrintDebugInfo("ERROR：查找的新闻无作者");
				return resp;
			}
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("查找新闻异常");
			Arale.PrintDebugInfo("ERROR：查找新闻异常" + e.getMessage());
			return resp;
		}
		news.setAuthor(author);
		//
		// return
		resp.setContent(news);
		return resp;
	}
	
	private Response<PageList<News>> helpGetNewsList(int type, PageInfo pgInfo, boolean isAdmin)
	{
		final Response<PageList<News>> resp = new Response<PageList<News>>();
		//
		// check paramters
		if(type != News.TYPE_NEWS && type != News.TYPE_NOTICE)
		{
			throw new IllegalArgumentException("news type not exist");
		}
		if(pgInfo == null)
		{
			throw new NullPointerException("pgInfo == null");
		}
		//
		// check limits
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		// get news list
		PageList<News> lst = null;
		try
		{
			lst = DaoFactory.createNewsDao().getNewsPageListByType(this.createPageSession(News.class, 5, 5), pgInfo, type, isAdmin);
		}
		catch(ConnectionException | ExecuteException e)
		{
			resp.setErrorInfo("分页查找失败");
			Arale.PrintDebugInfo("ERROR：分页查找异常" + e.getMessage());
			return resp;
		}
		//
		// adjust news order by whether the news is sticked
		Collections.sort(lst, new NewsComparator());
		//
		// return
		resp.setContent(lst);
		return resp;
	}
	
	@Override
	public Response<News> getStaticNews(Integer newsId)
	{
		Response<News> resp = new Response<News>();
		/*
		 * check parameters
		 */
		if(newsId > 10)
		{
			resp.setErrorInfo("错误的请求");
			return resp;
		}
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		//
		News news = null;
		try
		{
			news = DaoFactory.createNewsDao().findById(newsId);
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("查询失败");
			return resp;
		}
		if(news == null)
		{
			resp.setErrorInfo("条目不存在");
			return resp;
		}
		//
		resp.setContent(news);
		return resp;
	}
	
	/**
	 * Servlet Method
	 */
	@Override
	public Response<List<News>> getMajorList()
	{
		Response<List<News>> resp = new Response<List<News>>();
		List<News> lst = null;
		try
		{
			lst = DaoFactory.createNewsDao().findList(News.STATUS + " = " + News.STATUS_PUBLISHED + " AND " + News.TYPE + " = " + News.TYPE_MAJOR, News.CREATE_TIME + " ASC ");
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("数据库错误");
			return resp;
		}
		if(lst == null)
		{
			resp.setErrorInfo("查询失败");
			return resp;
		}
		resp.setContent(lst);
		return resp;
	}
	
	@Override
	public Response<List<News>> getXNoticeList()
	{
		Response<List<News>> resp = new Response<List<News>>();
		List<News> lst = null;
		try
		{
			lst = DaoFactory.createNewsDao().findList(News.STATUS + " = " + News.STATUS_PUBLISHED + " AND " + News.TYPE + " = " + News.TYPE_XNOTICE, News.CREATE_TIME + " DESC ");
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("数据库错误");
			return resp;
		}
		if(lst == null)
		{
			resp.setErrorInfo("查询失败");
			return resp;
		}
		resp.setContent(lst);
		return resp;
	}
}
