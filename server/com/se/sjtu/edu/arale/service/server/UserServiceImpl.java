/**
 * UserServiceImpl.java
 */
package com.se.sjtu.edu.arale.service.server;

import com.se.sjtu.edu.arale.config.Arale;
import com.se.sjtu.edu.arale.dao.DaoFactory;
import com.se.sjtu.edu.arale.service.client.IUserService;
import com.se.sjtu.edu.arale.service.validator.LoginValidator;
import com.se.sjtu.edu.arale.session.UserSession;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.browser.util.client.TextUtil;
import edu.iao.yomi.dao.exception.ConnectionException;
import edu.iao.yomi.dao.exception.ExecuteException;
import edu.iao.yomi.service.Method;
import edu.iao.yomi.service.Service;
import edu.iao.yomi.vo.resp.Response;

/**
 * @author lansunlong
 * @since 2013年12月1日
 */
public class UserServiceImpl extends Service implements IUserService
{
	private static final long serialVersionUID = 1L;
	
	@Method(validator = LoginValidator.class)
	@Override
	public Response<User> getCurrentUser()
	{

		Response<User> resp = new Response<User>();
		String forbidden = null;
		//
		// check limits
		forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			return resp;
		}
		//
		//
		User user = new UserSession(this.getThreadLocalRequest().getSession()).getCurrentUser();
		resp.setContent(user);
		return resp;
	}
	
	@Override
	public Response<String> login(String username, String pass)
	{

		Response<String> resp = new Response<String>();
		String forbidden = null;
		//
		// check params
		//
		if(TextUtil.isNullOrZero(username))
		{
			resp.setErrorInfo("用户名不能为空");
			return resp;
		}
		if(TextUtil.isNullOrZero(pass))
		{
			resp.setErrorInfo("密码不能为空");
			return resp;
		}
		//
		// validator
		//
		forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo("登录错误");
			Arale.PrintDebugInfo("ERROR：" + "登录错误" + forbidden + ",用户名:" + username + ", 密码:" + pass);
			return resp;
		}
		//
		//
		//
		User user = new User();
		user.setId(null);
		user.setUsername(username);
		try
		{
			user = DaoFactory.createUserDao().find(user);
		}
		catch(ExecuteException | ConnectionException e)
		{
			resp.setErrorInfo("登录错误");
			Arale.PrintDebugInfo("ERROR：" + "查询用户失败" + ",用户名:" + username + ", 密码:" + pass);
			return resp;
		}
		if(user == null)
		{
			resp.setErrorInfo("用户不存在");
			return resp;
		}
		if(!user.getPassword().equals(pass))
		{
			resp.setErrorInfo("密码不正确");
			return resp;
		}
		//
		new UserSession(this.getThreadLocalRequest().getSession()).setCurrentUserId(user.getId());
		//
		resp.setContent("登录成功");
		return resp;
	}
	
	@Override
	public Response<String> logout()
	{
	
		Response<String> resp = new Response<String>();
		//
		String forbidden = this.validateLimits();
		if(forbidden != null)
		{
			resp.setErrorInfo(forbidden);
			Arale.PrintDebugInfo("ERROR：退出错误，" + forbidden);
			return resp;
		}
		//
		// 获取用户记录
		UserSession userSession = new UserSession(this.getThreadLocalRequest().getSession());
		User user = userSession.getCurrentUser();
		if(user == null)
		{
			resp.setContent("已经退出登录");
			return resp;
		}
		//
		// 执行注销（从 Session中删除用户）
		userSession.removeCurrentUser();
		//
		// 返回结果
		resp.setContent("退出成功");
		return resp;
	}
}
