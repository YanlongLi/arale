/**
 * ViewCountServlet.java
 */
package com.se.sjtu.edu.arale.service.sevlet;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.se.sjtu.edu.arale.config.Arale;
import com.se.sjtu.edu.arale.session.ViewCountSession;

/**
 * @author lansunlong
 * @since 2013年12月2日
 */
public class ViewCountServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	/*
	 * Cookie
	 */
	public static final int COOKIE_EXPIRE_HOURS = 1;
	// public static final long MILLISECS_PER_HOUR = 1000L * 60L * 60L;
	public static final int MILLISECS_PER_HOUR = 60;
	
	private static String getIpAddr(HttpServletRequest request)
	{
		String ip = request.getHeader("x-forwarded-for");
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getRemoteAddr();
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("http_client_ip");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		// 如果是多级代理，那么取第一个ip为客户ip
		if(ip != null && ip.indexOf(",") != -1)
		{
			ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		}
		return ip;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		//
		Integer sessionCount = new ViewCountSession(req.getSession()).getViewCount();
		if(sessionCount != null)
		{
			resp.getWriter().write(Long.toString(sessionCount.intValue()));
			return;
		}
		ServletContext context = getServletContext();
		Integer count = null;
		synchronized(context)
		{
			count = (Integer)context.getAttribute(Arale.CONTEX_VIEW_COUNT);
			if(count == null)
			{
				count = new Integer(1);
			}
			else
			{
				count = new Integer(count.intValue() + 1);
			}
			context.setAttribute(Arale.CONTEX_VIEW_COUNT, count);
		}
		new ViewCountSession(req.getSession()).setViewCount(count);
		Arale.PrintDebugInfo("INFO：IP:" + ViewCountServlet.getIpAddr(req) + ", Count：" + count.intValue());
		resp.getWriter().write(Long.toString(count.intValue()));
		return;
	}
}
