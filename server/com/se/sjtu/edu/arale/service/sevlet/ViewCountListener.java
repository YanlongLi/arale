package com.se.sjtu.edu.arale.service.sevlet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.se.sjtu.edu.arale.config.Arale;

public class ViewCountListener implements ServletContextListener
{
	public ViewCountListener()
	{
	}
	
	@Override
	public void contextInitialized(ServletContextEvent arg0)
	{
		ServletContext context = arg0.getServletContext();
		Integer count = null;
		//
		String path = context.getRealPath(Arale.PATH_FOR_VIEWCOUNT);
		File file = new File(path);
		if(!file.exists())
		{
			try
			{
				file.createNewFile();
			}
			catch(IOException e)
			{
				Arale.PrintDebugInfo("Error：创建访问计数文件" + path + "失败");
				e.printStackTrace();
			}
		}
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			count = Integer.parseInt(reader.readLine());
			reader.close();
		}
		catch(IOException e)
		{
			Arale.PrintDebugInfo("ERROR：初始化访问计数失败");
			e.printStackTrace();
		}
		catch(NumberFormatException e)
		{
			Arale.PrintDebugInfo("ERROR：访问计数无法解析，初始化为123456");
			count = new Integer(123456);
		}
		Arale.PrintDebugInfo("INFO：ServletContexInit");
		context.setAttribute(Arale.CONTEX_VIEW_COUNT, count);
		return;
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0)
	{
		ServletContext context = arg0.getServletContext();
		Integer count = (Integer)arg0.getServletContext().getAttribute(Arale.CONTEX_VIEW_COUNT);
		if(count == null)
		{
			return;
		}
		BufferedWriter writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(context.getRealPath(Arale.PATH_FOR_VIEWCOUNT)))));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try
		{
			writer.write(Integer.toString(count));
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return;
	}
}
