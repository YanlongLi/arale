/**
 * LoginValidator.java
 */
package com.se.sjtu.edu.arale.service.validator;

import javax.servlet.http.HttpServletRequest;

import com.se.sjtu.edu.arale.session.UserSession;
import com.se.sjtu.edu.arale.vo.client.User;

import edu.iao.yomi.service.validator.ALimitsValidator;

/**
 * @author lansunlong
 * @since 2013年12月2日
 */
public class LoginValidator extends ALimitsValidator
{
	@Override
	public String doValidate(HttpServletRequest req, Object... param)
	{
		User user = new UserSession(req.getSession()).getCurrentUser(false);
		if(user == null)
		{
			return "当前用户没有登录";
		}
		return null;
	}
}
