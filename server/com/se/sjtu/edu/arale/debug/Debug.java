/**
 * Debug.java
 */
package com.se.sjtu.edu.arale.debug;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lansunlong
 * @since 2013年12月4日
 */
public class Debug
{
	public static final boolean DEBUG = true;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	private Debug()
	{
		return;
	}
	
	public static void PrintDebugInfo(Object... objects)
	{
		StringBuffer sb = new StringBuffer();
		if(DEBUG)
		{
			sb.append("File:" + _FILE_() + ", Line:" + _LINE_() + ", Function:" + _FUNC_());
		}
		for(int i = 0; i < objects.length; i++)
		{
			sb.append(",").append(objects[i]);
		}
		System.out.println(sb.toString());
	}
	
	public static String getFileLineMethod()
	{
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		StringBuffer toStringBuffer = new StringBuffer("[").append(traceElement.getFileName()).append(" | ").append(traceElement.getLineNumber()).append(" | ").append(traceElement.getMethodName()).append("]");
		return toStringBuffer.toString();
	}
	
	// 当前文件名
	public static String _FILE_()
	{
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getFileName();
	}
	
	// 当前方法名
	public static String _FUNC_()
	{
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getMethodName();
	}
	
	// 当前行号
	public static int _LINE_()
	{
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getLineNumber();
	}
	
	// 当前时间
	public static String _TIME_()
	{
		Date now = new Date();
		return sdf.format(now);
	}
}
