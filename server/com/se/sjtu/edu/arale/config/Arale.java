package com.se.sjtu.edu.arale.config;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.iao.yomi.config.Config;
import edu.iao.yomi.dao.DbConnector;

public class Arale extends Config
{
	public static final String SESSION_VIEW_COUNT = "view_count";
	public static final String CONTEX_VIEW_COUNT = "view_count";
	public static final String PATH_FOR_VIEWCOUNT = "WEB-INF" + File.separator + "ViewCount";
	public static final String DOMAIN_NAME = "202.120.40.32";
	//
	public static final Boolean DEBUG = true;
	public static final Boolean LOG = true;
	public static SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
	
	public static void WriteLog(String info)
	{
		if(Arale.LOG)
		{
			System.out.println("LOG:" + info);
		}
	}
	
	public static void PrintDebugInfo(String info)
	{
		if(Arale.DEBUG)
		{
			System.out.println(Arale.sdf.format(new Date()) + "-" + info);
		}
	}
	
	/*
	 * Database Connector
	 */
	private static final String dbUrl = "jdbc:postgresql://localhost/arale_db";
	private static final String dbUser = "arale";
	private static final String dbPwd = "";
	public static final DbConnector DB_CONNECTOR_FOR_ARALE = new DbConnector(Config.DB_DRIVER_FOR_POSTGRESQL, Arale.dbUrl, Arale.dbUser, Arale.dbPwd);
}
